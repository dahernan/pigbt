#!/usr/bin/env python3
###############################################################################
#                                                                             #
#        _        _____ ____ _______     ____     _____ ____ _______          #
#       | |      / ____|  _ \__   __|   |    \ _ / ____|  _ \__   __|         #
#       | |_ __ | |  __| |_) | | |______| |_) |_| |  __| |_) | | |            #
#       | | '_ \| | |_ |  _ <  | |______| ___/ _| | |_ |  _ <  | |            #
#       | | |_) | |__| | |_) | | |      | |   | | |__| | |_) | | |            #
#       |_| .__/ \_____|____/  |_|      |_|   |_|\_____|____/  |_|            #
#         | |                                                                 #
#         |_|                                                                 #
#                                                                             #
#  Copyright (C) 2021 lpGBT Team, CERN                                        #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################


from flask import Blueprint
from flask import request
from flask import send_file
from werkzeug.utils import secure_filename
from .lpgbtLib.lpgbt_low_level import LpgbtLowLevel as LpgbtLowLevel
from .lpgbtLib.pigbt import PiGBT as Lpgbt
from . import synchDecorator
import datetime
import time
bp = Blueprint('api', __name__, url_prefix='/api')
mode = 0
synchDecorator.init(bp)


def init_routes():
    bp.add_url_rule('/eye', 'eye', eye, methods=('GET',))
    bp.add_url_rule('/eye/download/<string:filename>',
                    'eye_download', eye_download, methods=('GET',))
    bp.add_url_rule('/connection', 'put_connection',
                    put_connection, methods=('PUT',))
    bp.add_url_rule('/connection', 'get_connection',
                    get_connection, methods=('GET',))
    bp.add_url_rule('/smbus', 'get_i2c_address',
                    get_i2c_address, methods=('GET',))
    bp.add_url_rule('/smbus', 'set_i2c_address',
                    set_i2c_address, methods=('PUT',))
    bp.add_url_rule('/bert', 'get_bert', get_bert, methods=('GET',))
    bp.add_url_rule('/chip_version', 'get_chip_version',
                    get_chip_version, methods=('GET',))
    bp.add_url_rule('/dataSources', 'get_data_sources',
                    get_data_sources, methods=('GET',))
    bp.add_url_rule('/dataSources', 'put_data_sources',
                    put_data_sources, methods=('PUT',))
    bp.add_url_rule('/regs/load', 'regs_load',
                    regs_load, methods=('GET', 'POST'))
    bp.add_url_rule('/regs/save', 'regs_save', regs_save, methods=('GET',))
    bp.add_url_rule('/regs/download/<string:filename>',
                    'regs_download', regs_download, methods=('GET',))
    bp.add_url_rule('/regs', 'regs_get', regs_get, methods=('GET',))
    bp.add_url_rule('/gpio', 'get_gpio', get_gpio, methods=('GET',))
    bp.add_url_rule('/gpio', 'put_gpio', put_gpio, methods=('PUT',))
    bp.add_url_rule('/readI2cMaster', 'put_read_I2Cm',
                    put_read_I2Cm, methods=('PUT',))
    bp.add_url_rule('/writeI2cMaster', 'put_write_I2Cm',
                    put_write_I2Cm, methods=('PUT',))
    bp.add_url_rule('/resetI2cMaster', 'put_reset_I2Cm',
                    put_reset_I2Cm, methods=('PUT',))
    bp.add_url_rule('/i2cMasterReadValue', 'get_i2c_master_read',
                    get_i2c_master_read, methods=('GET',))
    bp.add_url_rule('/regsm/save', 'regsm_save',
                    regsm_save, methods=('GET', 'POST'))
    bp.add_url_rule('/regsm/load', 'regsm_load',
                    regsm_load, methods=('GET', 'POST'))
    bp.add_url_rule('/regs/download/<string:filename>',
                    'regsm_download', regsm_download, methods=('GET',))
    bp.add_url_rule('/write',  'write', write, methods=('PUT',))
    bp.add_url_rule('/adc', 'get_adc', get_adc, methods=('GET',))
    bp.add_url_rule('/adc', 'put_adc', put_adc, methods=('PUT',))
    bp.add_url_rule('/cdac', 'get_cdac', get_cdac, methods=('GET',))
    bp.add_url_rule('/cdac', 'put_cdac', put_cdac, methods=('PUT',))
    bp.add_url_rule('/vdac', 'get_vdac', get_vdac, methods=('GET',))
    bp.add_url_rule('/vdac', 'put_vdac', put_vdac, methods=('PUT',))
    bp.add_url_rule('/vref', 'get_vref', get_vref, methods=('GET',))
    bp.add_url_rule('/vref', 'put_vref', put_vref, methods=('PUT',))
    bp.add_url_rule('/internalSensorTemp', 'get_internal_temp',
                    get_internal_temp, methods=('GET',))
    bp.add_url_rule('/vddMonitoring', 'put_vdd_mon',
                    put_vdd_mon, methods=('PUT',))
    bp.add_url_rule('/equalizer', 'get_equalizer',
                    get_equalizer, methods=('GET',))
    bp.add_url_rule('/equalizer', 'set_equalizer',
                    set_equalizer, methods=('PUT',))
    bp.add_url_rule('/lineDriver', 'get_line_driver',
                    get_line_driver, methods=('GET',))
    bp.add_url_rule('/lineDriver', 'set_line_driver',
                    set_line_driver, methods=('PUT',))
    bp.add_url_rule('/hsPolarity', 'get_hs_polarity',
                    get_hs_polarity, methods=('GET',))
    bp.add_url_rule('/hsPolarity', 'set_hs_polarity',
                    set_hs_polarity, methods=('PUT',))
    bp.add_url_rule('/clocks', 'get_clocks', get_clocks, methods=('GET',))
    bp.add_url_rule('/eClocks', 'put_eclocks', put_eclocks, methods=('PUT',))
    bp.add_url_rule('/psClocks', 'put_psclocks',
                    put_psclocks, methods=('PUT',))
    bp.add_url_rule('/eptxAdvanced', 'get_eptx_advanced',
                    get_eptx_advanced, methods=('GET',))
    bp.add_url_rule('/eptxAdvanced', 'put_eptx_advanced',
                    put_eptx_advanced, methods=('PUT',))
    bp.add_url_rule('/eptx', 'get_eptx', get_eptx, methods=('GET',))
    bp.add_url_rule('/eptx', 'put_eptx', put_eptx, methods=('PUT',))
    bp.add_url_rule('/eptxEc', 'put_eptx_ec', put_eptx_ec, methods=('PUT',))
    bp.add_url_rule('/eptxEc', 'get_eptx_ec', get_eptx_ec, methods=('GET',))
    bp.add_url_rule('/eprxAdvanced', 'get_eprx_advanced',
                    get_eprx_advanced, methods=('GET',))
    bp.add_url_rule('/eprxAdvanced', 'put_eprx_advanced',
                    put_eprx_advanced, methods=('PUT',))
    bp.add_url_rule('/eprxRetrainChn', 'put_eprx_retrain_channel',
                    put_eprx_retrain_channel, methods=('PUT',))
    bp.add_url_rule('/eprxEc', 'put_eprx_ec', put_eprx_ec, methods=('PUT',))
    bp.add_url_rule('/eprxEc', 'get_eprx_ec', get_eprx_ec, methods=('GET',))
    bp.add_url_rule('/eprxLock', 'get_eprx_lock',
                    get_eprx_lock, methods=('GET',))
    bp.add_url_rule('/eprxPhases', 'get_eprx_phases',
                    get_eprx_phases, methods=('GET',))
    bp.add_url_rule('/eprx', 'get_eprx', get_eprx, methods=('GET',))
    bp.add_url_rule('/eprx', 'put_eprx', put_eprx, methods=('PUT',))
    bp.add_url_rule('/eprxDll', 'get_eprx_dll', get_eprx_dll, methods=('GET',))
    bp.add_url_rule('/pusmstate', 'get_pusmstatus',
                    get_pusmstatus, methods=('GET',))
    bp.add_url_rule('/resetOut', 'get_reset_config',
                    get_reset_config, methods=('GET',))
    bp.add_url_rule('/status', 'get_status', get_status, methods=('GET',))
    bp.add_url_rule('/mode', 'put_mode', put_mode, methods=('PUT',))
    bp.add_url_rule('/mode', 'get_mode', get_mode, methods=('GET',))
    bp.add_url_rule('/quickstart', 'quick_start',
                    quick_start, methods=('GET',))
    bp.add_url_rule('/jumptostate', 'jump_to_state',
                    jump_to_state, methods=('GET',))
    bp.add_url_rule('/getReadyState', 'get_ready_state',
                    get_ready_state, methods=('GET',))
    bp.add_url_rule('/resetOut', 'put_reset_out',
                    put_reset_out, methods=('PUT',))
    bp.add_url_rule('/reset', 'reset', reset, methods=['GET', 'POST'])
    bp.add_url_rule('/procMonStatus', 'get_process_monitor',
                    get_process_monitor, methods=('GET',))
    bp.add_url_rule('/byPassDatapath', 'get_bypass_datapath',
                    get_bypass_datapath, methods=('GET',))
    bp.add_url_rule('/byPassDatapath', 'put_bypass_datapath',
                    put_bypass_datapath, methods=('PUT',))
    bp.add_url_rule('/testOuptuts', 'get_test_outputs',
                    get_test_outputs, methods=('GET',))
    bp.add_url_rule('/testOuptuts', 'put_test_outputs',
                    put_test_outputs, methods=('PUT',))
    bp.add_url_rule('/feastControl', 'get_feast_control',
                    get_feast_control, methods=('GET',))
    bp.add_url_rule('/feastControl', 'put_feast_control',
                    put_feast_control, methods=('PUT',))
    bp.add_url_rule('/burnBankStatus', 'burn_bank_status',
                    burn_bank_status, methods=['GET', 'POST'])
    bp.add_url_rule('/fusesBurnBank', 'fuses_burn_bank',
                    fuses_burn_bank, methods=['GET', 'POST'])
    bp.add_url_rule('/watchdog', 'get_watchdog_config',
                    get_watchdog_config, methods=('GET',))
    bp.add_url_rule('/watchdog', 'put_watchdog_config',
                    put_watchdog_config, methods=('PUT',))
    bp.add_url_rule('/resetWatchdog', 'put_reset_watchdog',
                    put_reset_watchdog, methods=('PUT',))
    bp.add_url_rule('/resetTimeout', 'put_reset_timeout',
                    put_reset_timeout, methods=('PUT',))


def json_get(request, field, value=0):
    if field in request.json:
        return request.json[field]
    else:
        return value


"""----------------------------------------API functions------------------------------------------"""


@synchDecorator.synch
def eye(lpgbtInst):

    filename = 'eye_{}.png'.format(
        datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    func = lpgbtInst.eye_opening_scan('eye/'+filename)

    return {'filename': filename}


def eye_download(filename):
    return send_file('eye/'+filename, attachment_filename=filename, as_attachment=True)


@synchDecorator.synch_pigbt
def put_connection(lpgbtInst):
    """
        Set lpGBT connection mode :
        Mockup - no lpGBT
        Raspberry Pi control - I2C direct
        I2C Master
    """

    lpgbtLowLevelInst = lpgbtInst.getLpgbtLowLevelInst()

    if not request.json:
        raise Exception('JSON field is not present')

    connection = json_get(request, "connection", 0)
    i2cm = json_get(request, "i2cm", 0)
    slave_addr = json_get(request, "slave_addr", 0)

    if connection == 0:
        lpgbtLowLevelInst.set_platform(LpgbtLowLevel.MOCKUP)

    elif connection == 1:
        lpgbtLowLevelInst.set_platform(LpgbtLowLevel.RPI)
        lpgbtInst.feast_control(True)
    elif connection == 2:
        lpgbtLowLevelInst.set_platform(LpgbtLowLevel.LPGBTMASTER, {
                                       'i2cm': i2cm, 'slave_addr': slave_addr})

    return {}


@synchDecorator.synch_pigbt
def get_connection(lpgbtInst):
    lpgbtLowLevelInst = lpgbtInst.getLpgbtLowLevelInst()
    platform = lpgbtLowLevelInst.get_platform()

    if platform == LpgbtLowLevel.MOCKUP:
        connection = 0
        chip_addr = 0
    elif platform == LpgbtLowLevel.RPI:
        lpgbtInst.feast_control(True)
        connection = 1
        chip_addr = lpgbtLowLevelInst.get_i2c_address()
    elif platform == LpgbtLowLevel.LPGBTMASTER:
        connection = 2
        chip_addr = 0
    else:
        raise Exception('Unknown platform')

    return {
        'connection': connection,
        'chip_addr': chip_addr
    }


@synchDecorator.synch_pigbt
def get_i2c_address(lpgbtInst):
    address = lpgbtInst.scan_smbus()
    return {'address': address}


@synchDecorator.synch_pigbt
def set_i2c_address(lpgbtInst):

    lpgbtLowLevelInst = lpgbtInst.getLpgbtLowLevelInst()

    if not request.json:
        raise Exception('JSON field is not present')

    address_selected = json_get(request, "address_selected", 0)
    chip_version = json_get(request, "chip_version", 0)

    lpgbtLowLevelInst.set_chip_version(chip_version)
    lpgbtLowLevelInst.set_i2c_address(address_selected)

    return {}


@synchDecorator.synch_pigbt
def get_chip_version(lpgbtInst):

    chip_version = lpgbtInst.detect_chip_version()
    return {'chip_version': chip_version}


@synchDecorator.synch
def reset(lpgbtInst):
    """
        Reset the 1.2 V DC-DC to LpGBT
    """
    lpgbtInst.generate_reset_pulse(0.01)
    return {}


@synchDecorator.synch
def burn_bank_status(lpgbtInst):

    fuse_data = []
    fuse_addr = []
    val = []
    n_registers = 240
    reg_bank = 4
    fused_bank_status = []

    for address in lpgbtInst.Reg:
        val.append(lpgbtInst.read_reg(address))

    for i in range(0, n_registers, reg_bank):
        if(sum(val[i:(i+4)])):
            fuse_addr.append(i)
            fuse_data.append(val[i+3] << 24 | val[i+2] <<
                             16 | val[i+1] << 8 | val[i] << 0)

    for i in range(0, n_registers, reg_bank):
        value = lpgbtInst.fuses_read_bank(i)
        if(value != 0x0):
            for addr in fuse_addr:
                if(i == addr):
                    fused_bank_status.append(
                        'You are going to fuse already fused bank '+str(i)+' value : '+str(hex(value)))
                    fuses_burned = True
                    status = False
                    break
    return {
        'fused_bank_status': fused_bank_status,
    }


@synchDecorator.synch_pigbt
def set_fuse_pin(lpgbtInst):
    """

    """
    lpgbtInst.control_fuse_pin(1)

    return {}


@synchDecorator.synch_pigbt
def reset_fuse_pin(lpgbtInst):
    """

    """
    lpgbtInst.control_fuse_pin(0)

    return {}


@synchDecorator.synch
def fuses_burn_bank(lpgbtInst):
    fuse_data = []
    fuse_addr = []
    val = []
    n_registers = 240
    reg_bank = 4

    for address in lpgbtInst.Reg:
        val.append(lpgbtInst.read_reg(address))

    for i in range(0, n_registers, reg_bank):
        if(sum(val[i:(i+4)])):
            fuse_addr.append(i)
            fuse_data.append(val[i+3] << 24 | val[i+2] <<
                             16 | val[i+1] << 8 | val[i] << 0)

    set_fuse_pin()
    time.sleep(0.01)
    for i in range(0, len(fuse_addr)):
        lpgbtInst.fuses_burn_bank(fuse_addr[i], fuse_data[i], 15)
    reset_fuse_pin()
    return {}


@synchDecorator.synch_pigbt
def put_feast_control(lpgbtInst):
    """

    """
    if not request.json:
        raise Exception('JSON field is not present')

    feast_enable = json_get(request, "feast_enable", 0)

    lpgbtInst.feast_control(feast_enable)

    return {}


@synchDecorator.synch_pigbt
def get_feast_control(lpgbtInst):

    feast_enable = bool(lpgbtInst.get_feast_control())
    return {
        'feast_enable': feast_enable,
    }


@synchDecorator.synch
def put_reset_out(lpgbtInst):
    """
        Active low reset signal is generated on the RSTOUTB pin
    """

    if not request.json:
        raise Exception('JSON field is not present')

    pulse_duration = json_get(request, "pulse_duration", 0)
    lpgbtInst.rstoutb_config(pulse_duration)

    return {}


@synchDecorator.synch
def quick_start(lpgbtInst):
    """
        LpGBT registers minimal configuration to initalize the Chip until ready state
        Initialization sequence : - Read Pause for PLL config state
                                  - Writes default configuration for Clock Generator
                                  - Configures line driver
                                  - Configures equalizer
                                  - Power up after PLL config done
                                  - Wait and Read ready state
    """

    lpgbtInst.clock_generator_setup()
    lpgbtInst.line_driver_setup()
    lpgbtInst.equalizer_setup(attenuation=3)
    lpgbtInst.config_done()

    time.sleep(3)

    if lpgbtInst.pusm_get_state() != lpgbtInst.PusmState.READY:
        check_watchdog_and_timeout()


@synchDecorator.synch
def check_watchdog_and_timeout(lpgbtInst):

    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        actions = lpgbtInst.read_reg(lpgbtInst.PUSMACTIONS)

        if actions & lpgbtInst.PUSMACTIONS.PUSMPLLTIMEOUTACTION.bit_mask:
            raise Exception('PLL timeout action detected. lpGBT reset.')

        if actions & lpgbtInst.PUSMACTIONS.PUSMDLLTIMEOUTACTION.bit_mask:
            raise Exception('DLL timeout action detected. lpGBT reset.')

    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        pusm_pll_timeout_counter = lpgbtInst.read_reg(
            lpgbtInst.PUSMPLLTIMEOUT.address)
        pusm_dll_timeout_counter = lpgbtInst.read_reg(
            lpgbtInst.PUSMDLLTIMEOUT.address)

        if pusm_pll_timeout_counter != 0:
            raise Exception('PLL timeout action detected. lpGBT reset.')

        if pusm_dll_timeout_counter != 0:
            raise Exception('DLL timeout action detected. lpGBT reset.')

    return {}


@synchDecorator.synch
def jump_to_state(lpgbtInst):
    """
          Controls behavior of the power up state machine to force Ready state.
    """
    lpgbtInst.pusm_jump_to_state(lpgbtInst.PusmState.READY, False)

    return {}


@synchDecorator.synch
def get_ready_state(lpgbtInst):
    pusm_get_state = lpgbtInst.pusm_get_state()
    ready_state = 0
    if pusm_get_state == lpgbtInst.PusmState.READY:
        ready_state = 1
    return {
        'ready_state': ready_state,
    }


@synchDecorator.synch
def get_mode(lpgbtInst):
    """
        GET mode : get chip mode configuration from CONFIGPINS register
        Data : Transceiver mode, data rate, fec
    """
    dr = '5'
    fec = '5'
    trx = 'off'

    val = lpgbtInst.read_mode()

    if val & 0x8:
        dr = "10"
    if val & 0x4:
        fec = "12"
    if val & 0x3 == 0:
        trx = "off"
    elif val & 0x3 == 1:
        trx = "tx"
    elif val & 0x3 == 2:
        trx = "rx"
    elif val & 0x3 == 3:
        trx = "trx"

    return {
        'mode': trx,
        'tx_fec': fec,
        'tx_datarate': dr,
    }


@synchDecorator.synch
def put_mode(lpgbtInst):
    """
        PUT request to send chip mode pins configuration to lpGBT
        Data : Transceiver mode, data rate, fec
    """
    if not request.json:
        raise Exception('JSON field is not present')

    trx = 'off'
    tx_fec = '5'
    tx_datarate = '5'
    if 'mode' in request.json:
        trx = request.json['mode']
    if 'tx_fec' in request.json:
        tx_fec = request.json['tx_fec']
    if 'tx_datarate' in request.json:
        tx_datarate = request.json['tx_datarate']

    new_mode = 0

    if tx_fec == '12':
        new_mode |= 0x4
    if tx_datarate == '10':
        new_mode |= 0x8
    if trx == 'off':
        new_mode = 0x0
    elif trx == 'tx':
        new_mode |= 0x1
    elif trx == 'rx':
        new_mode |= 0x2
    elif trx == 'trx':
        new_mode |= 0x3

    lpgbtInst.set_mode_pins(new_mode)

    return {'mode': mode}


@synchDecorator.synch
def get_status(lpgbtInst):
    """
        GET LpGBT status if the chip address is detected on RPI I2C bus
        Status informations : - Chip I2C address
                              - Chip ID
                              - Transceiver Mode
                              - Chip lockmode
                              - State machine value
    """

    address = lpgbtInst.read_reg(lpgbtInst.I2CSLAVEADDRESS.address)
    try:
        chipid = lpgbtInst.get_chipid_fuses()["chipid"]
    except:
        chipid = 0

    configpins = lpgbtInst.read_reg(lpgbtInst.CONFIGPINS.address)
    mode = configpins >> lpgbtInst.CONFIGPINS.LPGBTMODE.offset

    mode = lpgbtInst.read_mode()
    lockmode = lpgbtInst.read_reg(
        lpgbtInst.CONFIGPINS.address) & lpgbtInst.CONFIGPINS.LOCKMODE.bit_mask
    pusmstate = lpgbtInst.pusm_get_state()

    switcher_lockmode = {
        0: "EXTERNAL REF CLK",
        2: "RECOVERED REF CLK",
    }

    switcher_chipmode = {
        0: "Transmission off",
        1: "MODE 5G FEC5 TX",
        2: "MODE 5G FEC5 RX",
        3: "MODE 5G FEC5 TRX",

        4: "Transmission off",
        5: "MODE 5G FEC12 TX",
        6: "RX",
        7: "MODE 5G FEC12 TRX",

        8: "Transmission off",
        9: "MODE 10G FEC5 TX",
        10: "RX",
        11: "MODE 10G FEC5 TRX",

        12: "Transmission off",
        13: "MODE 10G FEC12 TX",
        14: "RX",
        15: "MODE 10G FEC12 TRX",
    }

    return {
        'address': address,
        'chipid': chipid,
        'mode': switcher_chipmode.get(lpgbtInst.read_mode(), "Invalid"),
        'lockmode': switcher_lockmode.get(lockmode, "Invalid"),
        'pusmstate': lpgbtInst.PusmState(pusmstate).name,
    }


@synchDecorator.synch
def get_reset_config(lpgbtInst):
    """
        GET LpGBT reset config
    """

    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        pulse_duration = (int(lpgbtInst.read_reg(lpgbtInst.RESETCONFIG))
                          >> lpgbtInst.RESETCONFIG.RESETOUTLENGTH.offset) & 0x3
    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        pulse_duration = (int(lpgbtInst.read_reg(lpgbtInst.RESETCONFIG))
                          >> lpgbtInst.RESETCONFIG.RESETOUTLENGTH.offset) & 0x3
    return {
        'pulse_duration': pulse_duration,
    }


@synchDecorator.synch
def get_pusmstatus(lpgbtInst):
    """
        GET LpGBT status if the chip address is detected on RPI I2C bus
    """
    @synchDecorator.synch_pigbt
    def set_green_led(lpgbtInst):
        lpgbtInst.rpi_control_pin(0, 1)

    @synchDecorator.synch_pigbt
    def set_red_led(lpgbtInst):
        lpgbtInst.rpi_control_pin(1, 1)

    @synchDecorator.synch_pigbt
    def reset_green_led(lpgbtInst):
        lpgbtInst.rpi_control_pin(0, 0)

    @synchDecorator.synch_pigbt
    def reset_red_led(lpgbtInst):
        lpgbtInst.rpi_control_pin(1, 0)

    device_status = 0
    try:
        device_status = lpgbtInst.pusm_get_state()
    except IndexError as e:
        pass

    if device_status == lpgbtInst.PusmState.READY:
        set_green_led()
        reset_red_led()
    elif device_status == lpgbtInst.PusmState.RESET0:
        reset_green_led()
        set_red_led()
    else:
        set_green_led()
        set_red_led()

    return {
        'device_status': device_status,
    }


@synchDecorator.synch
def put_eprx(lpgbtInst):
    """
        PUT request to configure EPRX group on global mode (all channels enabled)
        Parameters : - Data rate
                     - Trac mode forced on EPORTRXMODE_AUTOSTARTUP
                     - Term : 100 ohm termination enable option
                     - AC Bias : common mode generation enable option
    """
    if not request.json:
        raise Exception('JSON field is not present')

    term = json_get(request, "term", 0)
    ac = json_get(request, "ac", 0)
    data_rate = json_get(request, "data_rate", 0)

    for group_id in range(0, 7):
        if data_rate == 0:
            lpgbtInst.eprx_group_setup(
                group_id, data_rate, lpgbtInst.EportRxPhaseSelectionMode.AUTOSTARTUP, False, False, False, False)  # channel active none
        elif data_rate == 1:
            # channel active 0,1,2 and 3
            lpgbtInst.eprx_group_setup(
                group_id, data_rate, lpgbtInst.EportRxPhaseSelectionMode.AUTOSTARTUP, True, True, True, True)
        elif data_rate == 2:
            lpgbtInst.eprx_group_setup(group_id, data_rate, lpgbtInst.EportRxPhaseSelectionMode.AUTOSTARTUP,
                                       True, False, True, False)   # channel active 0 and 2
        elif data_rate == 3:
            lpgbtInst.eprx_group_setup(
                group_id, data_rate, lpgbtInst.EportRxPhaseSelectionMode.AUTOSTARTUP, True, False, False, False)  # channel active 0

        for channel_id in range(0, 4):
            lpgbtInst.eprx_channel_config(
                group_id, channel_id, term, ac, False, 0, 0)

    return {'eprx mode': mode}


@synchDecorator.synch
def get_eprx(lpgbtInst):
    """
        GET EPRX group register configuration
    """

    term = ''
    ac = ''
    eprx_data_rate = (lpgbtInst.read_reg(lpgbtInst.EPRX0CONTROL.address)
                      >> lpgbtInst.EPRX0CONTROL.EPRX0DATARATE.offset) & 0x3
    chns = int(lpgbtInst.read_reg(lpgbtInst.EPRX00CHNCNTR.address))

    if chns & 0x02:
        term = 'term'
    if chns & 0x04:
        ac = 'ac'
    eprx_chns = [ac, term]

    return {
        'eprx_data_rate': eprx_data_rate,
        'eprx_chns': eprx_chns,
    }


@synchDecorator.synch
def put_eprx_advanced(lpgbtInst):
    """
        PUT request to set EPRX group advanced configuration
        Parameters : - Data rate
                     - Trac mode
                     - Equalizer
                     - Channels configuration for each group (ac bias, term, invert)
                     - Phase
    """

    if not request.json:
        raise Exception('JSON field is not present')

    eprx_data_rate_group = json_get(request, "eprx_data_rate_group", 0)
    eprx_track_mode_group = json_get(request, "eprx_track_mode_group", 0)
    eprx_chns_group = json_get(request, "eprx_chns_group", 0)
    eprx_equalization_chns_group = json_get(
        request, "eprx_equalization_chns_group", 0)
    eprx_chns_group_phase = json_get(request, "eprx_chns_group_phase", 0)
    eprx_chns_enable = json_get(request, "eprx_chns_enable", 0)

    for group_id in range(0, 7):
        lpgbtInst.eprx_group_setup(group_id, eprx_data_rate_group[group_id], eprx_track_mode_group[group_id], eprx_chns_enable[group_id]
                                   [0], eprx_chns_enable[group_id][1], eprx_chns_enable[group_id][2], eprx_chns_enable[group_id][3])

        for channel_id in range(0, 4):

            ac = True if 'ac' in eprx_chns_group[group_id][channel_id] else False
            term = True if 'term' in eprx_chns_group[group_id][channel_id] else False
            inv = True if 'inv' in eprx_chns_group[group_id][channel_id] else False

            lpgbtInst.eprx_channel_config(group_id, channel_id, term, ac, inv, int(
                eprx_chns_group_phase[group_id][channel_id]), eprx_equalization_chns_group[group_id][channel_id])

    return {}


@synchDecorator.synch
def put_eprx_retrain_channel(lpgbtInst):
    """Performs training on a single channel of a given ePortRx group"""

    if not request.json:
        raise Exception('JSON field is not present')

    eprx_chns_phase_training = json_get(request, "eprx_chns_phase_training", 0)

    for grp in range(len(eprx_chns_phase_training)):
        for chn in range(len(eprx_chns_phase_training[grp])):
            value = eprx_chns_phase_training[grp][chn]
            if value == 1:
                lpgbtInst.eprx_retrain_channel(grp, chn)


@synchDecorator.synch
def get_eprx_advanced(lpgbtInst):
    """
        GET EPRX group register configuration
    """

    eprx_data_rate_group = []
    eprx_track_mode_group = []
    eprx_chns = []
    eprx_chns_group = []
    eprx_chns_enable = []
    equalizer = []
    term = ''
    ac_bias = ''
    invert = ''
    phase_training = []
    eprx_chns_phase_training = []

    for group_id in range(0, 7):

        control_reg = lpgbtInst.read_reg(
            lpgbtInst.EPRX0CONTROL.address + group_id)
        dr = (control_reg >> lpgbtInst.EPRX0CONTROL.EPRX0DATARATE.offset) & 0x3
        tm = (control_reg) & 0x3
        phase_training_reg = lpgbtInst.EPRXTRAIN10.address + int(group_id/2)

        eprx_data_rate_group.append(dr)
        eprx_track_mode_group.append(tm)

        enabled = [bool(control_reg & 0x10), bool(control_reg & 0x20), bool(
            control_reg & 0x40), bool(control_reg & 0x80)]
        eprx_chns_enable.append(enabled)

        eq0 = lpgbtInst.read_reg(
            int(lpgbtInst.EPRXEQ10CONTROL.address + group_id/2))
        eq0 >>= 4*(group_id % 2)

        for channel_id in range(4):
            chn_cntr = lpgbtInst.read_reg(
                lpgbtInst.EPRX00CHNCNTR.address + group_id*4 + channel_id)

            equ1 = (lpgbtInst.read_reg(lpgbtInst.EPRX00CHNCNTR.address +
                    group_id*4 + channel_id)) & lpgbtInst.EPRX00CHNCNTR.EPRX00EQ.bit_mask

            req_eq = lpgbtInst.EPRXEQ10CONTROL.address + int(group_id/2)

            reg_val = lpgbtInst.read_reg(req_eq)

            if reg_val & (1 << (group_id*4+channel_id) % 8):
                equ0 = 1
            else:
                equ0 = 0

            if equ1 == 1 and equ0 == 1:
                eq = 3
            elif equ1 == 1 and equ0 == 0:
                eq = 2
            elif equ1 == 0 and equ0 == 1:
                eq = 1
            else:
                eq = 0

            chn_cntr = lpgbtInst.read_reg(
                lpgbtInst.EPRX00CHNCNTR.address + group_id*4 + channel_id)

            if chn_cntr & lpgbtInst.EPRX00CHNCNTR.EPRX00INVERT.bit_mask:
                invert = 'inv'
            else:
                invert = ''
            if chn_cntr & lpgbtInst.EPRX00CHNCNTR.EPRX00TERM.bit_mask:
                term = 'term'
            else:
                term = ''
            if chn_cntr & lpgbtInst.EPRX00CHNCNTR.EPRX00ACBIAS.bit_mask:
                ac_bias = 'ac'
            else:
                ac_bias = ''

            equalizer.append(eq)
            chns = [term, ac_bias, invert]
            eprx_chns.append(chns)

            phase_training_value = lpgbtInst.read_reg(phase_training_reg)
            phase_training.append(phase_training_value)

    eprx_chns_group = [eprx_chns[i:i+4] for i in range(0, len(eprx_chns), 4)]
    eprx_equalization_chns_group = [equalizer[i:i+4]
                                    for i in range(0, len(equalizer), 4)]
    eprx_chns_phase_training = [phase_training[i:i+4]
                                for i in range(0, len(phase_training), 4)]

    return {
        'eprx_data_rate_group': eprx_data_rate_group,
        'eprx_track_mode_group': eprx_track_mode_group,
        'eprx_chns_group': eprx_chns_group,
        'eprx_equalization_chns_group': eprx_equalization_chns_group,
        'eprx_chns_enable': eprx_chns_enable,
        'eprx_chns_phase_training': eprx_chns_phase_training,
    }


@synchDecorator.synch
def get_eprx_phases(lpgbtInst):
    """
        GET EPRX group register configuration
    """

    eprx_chns_group_phase = []
    eprx_chns_phase = []

    for group_id in range(0, 7):
        for channel_id in range(0, 3):
            eprx_chns_phase.append(
                lpgbtInst.eprx_channel_phase(group_id, channel_id))
        eprx_chns_group_phase.append(eprx_chns_phase)

    return {
        'eprx_chns_group_phase': eprx_chns_group_phase,
    }


@synchDecorator.synch
def get_eprx_lock(lpgbtInst):
    """
        GET EPRX group register configuration
    """
    eprx_chns_locked = []

    for group_id in range(0, 7):

        locked_reg = lpgbtInst.read_reg(
            lpgbtInst.EPRX0LOCKED.address + 3*group_id)
        locked = [bool(locked_reg & 0x10), bool(locked_reg & 0x20),
                  bool(locked_reg & 0x40), bool(locked_reg & 0x80)]
        eprx_chns_locked.append(locked)

    return {
        'eprx_chns_locked': eprx_chns_locked,
    }


@synchDecorator.synch
def get_eprx_dll(lpgbtInst):
    """
        GET EPRX DLL status
    """
    eprx_dll_locked = []
    eprx_lossoflock_counter = []
    eprx_filter_state = []

    for group_id in range(0, 7):
        locked_reg = lpgbtInst.read_reg(
            lpgbtInst.EPRX0LOCKED.address + 3 * group_id)
        status = locked_reg & 0x3
        dll_status = lpgbtInst.read_reg(
            lpgbtInst.EPRX0DLLSTATUS.address + group_id)
        eprx_dll_locked.append(
            bool(dll_status & lpgbtInst.EPRX0DLLSTATUS.EPRX0DLLLOCKED.bit_mask))

        eprx_filter_state.append(
            (dll_status >> lpgbtInst.EPRX0DLLSTATUS.EPRX0DLLLFSTATE.offset) & 0x3)
        eprx_lossoflock_counter.append(
            (dll_status >> lpgbtInst.EPRX0DLLSTATUS.EPRX0DLLLOLCNT.offset) & 0x1F)

    return {
        'eprx_dll_locked': eprx_dll_locked,
        'eprx_filter_state': eprx_filter_state,
        'eprx_lossoflock_counter': eprx_lossoflock_counter,
    }


@synchDecorator.synch
def put_eprx_ec(lpgbtInst):
    """
        PUT request to set the external Control (EC) ePortRx
        Parameters :  - enable: state of the EC ePortRx
                      - track_mode: phase selection mode (EPORTRXMODE_FIXED, EPORTRXMODE_CONTINOUSTRACKING, ...)
                      - term: input termination control
                      - ac_bias: AC bias generation control
                      - invert: data inversion control
                      - phase: EC static phase selection
    """

    if not request.json:
        raise Exception('JSON field is not present')

    eprx_ec_enable = json_get(request, "eprx_ec_enable", 0)
    eprx_ec_track_mode = json_get(request, "eprx_ec_track_mode", 0)
    eprx_ec_chns = json_get(request, "eprx_ec_chns", 0)
    eprx_ec_chn_phase = json_get(request, "eprx_ec_chn_phase", 0)
    ac = True if 'ac' in eprx_ec_chns else False
    term = True if 'term' in eprx_ec_chns else False
    inv = True if 'inv' in eprx_ec_chns else False

    lpgbtInst.eprx_ec_setup(enable=eprx_ec_enable, term=term,
                            ac_bias=ac,
                            invert=inv,
                            phase=int(eprx_ec_chn_phase),
                            track_mode=eprx_ec_track_mode,
                            )

    return {}


@synchDecorator.synch
def get_eprx_ec(lpgbtInst):
    """
        GET EPTX group register configuration
    """

    eprx_ec_enable = True
    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        eprx_ec_enable = lpgbtInst.read_reg(
            lpgbtInst.EPRXECCHNCNTR.address) & lpgbtInst.EPRXECCHNCNTR.EPRXECENABLE.bit_mask
    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        eprx_ec_enable = lpgbtInst.read_reg(
            lpgbtInst.EPRXECCONTROL.address) >> lpgbtInst.EPRXECCONTROL.EPRXECENABLE.offset

    eprx_ec_track_mode = lpgbtInst.read_reg(
        lpgbtInst.EPRXECCONTROL.address) & lpgbtInst.EPRXECCONTROL.EPRXECTRACKMODE.bit_mask
    eprx_ec_option = lpgbtInst.read_reg(lpgbtInst.EPRXECCHNCNTR.address)
    eprx_ec_chn_phase = (lpgbtInst.read_reg(
        lpgbtInst.EPRXECCHNCNTR.address) >> lpgbtInst.EPRXECCHNCNTR.EPRXECPHASESELECT.offset)

    term = ''
    ac = ''
    inv = ''
    if eprx_ec_option & lpgbtInst.EPRXECCHNCNTR.EPRXECTERM.bit_mask:
        term = 'term'
    if eprx_ec_option & lpgbtInst.EPRXECCHNCNTR.EPRXECACBIAS.bit_mask:
        ac = 'ac'
    if eprx_ec_option & lpgbtInst.EPRXECCHNCNTR.EPRXECINVERT.bit_mask:
        inv = 'inv'

    eprx_ec_chns = [term, ac, inv]

    return {
        'eprx_ec_enable': eprx_ec_enable,
        'eprx_ec_track_mode': eprx_ec_track_mode,
        'eprx_ec_chns': eprx_ec_chns,
        'eprx_ec_chn_phase': eprx_ec_chn_phase,
    }


@synchDecorator.synch
def put_eptx_ec(lpgbtInst):
    """
        PUT request to set the external Control (EC) ePortRx
        Parameters :  - enable: state of the EC ePortRx
                      - track_mode: phase selection mode (EPORTRXMODE_FIXED, EPORTRXMODE_CONTINOUSTRACKING, ...)
                      - term: input termination control
                      - ac_bias: AC bias generation control
                      - invert: data inversion control
                      - phase: EC static phase selection
    """

    if not request.json:
        raise Exception('JSON field is not present')

    eptx_ec_enable = json_get(request, "eptx_ec_enable", 0)
    etx_ec_ds = json_get(request, "etx_ec_ds", 0)
    etx_ec_pem = json_get(request, "etx_ec_pem", 0)
    etx_ec_pes = json_get(request, "etx_ec_pes", 0)
    etx_ec_pew = json_get(request, "etx_ec_pew", 0)
    eptx_invert = json_get(request, "eptx_invert", 0)

    lpgbtInst.eptx_ec_setup(
        enable=eptx_ec_enable,
        drive_strength=int(etx_ec_ds),
        pre_emphasis_mode=int(etx_ec_pem),
        pre_emphasis_strength=int(etx_ec_pes),
        pre_emphasis_width=int(etx_ec_pew),
        invert=eptx_invert,
    )

    return {}


@synchDecorator.synch
def get_eptx_ec(lpgbtInst):
    """
        GET EPTX group register configuration
    """
    eptx_ec_enable = False
    etx_ec_pem = 0
    etx_ec_pes = 0
    etx_ec_pew = 0
    eptx_invert = 0
    etx_ec_ds = 0

    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        eptx_ec_enable = (lpgbtInst.read_reg(
            lpgbtInst.EPTXCONTROL.address) >> lpgbtInst.EPTXCONTROL.EPTXECENABLE.offset) & 0x1
        etx_ec_pem = (lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR.address) >> lpgbtInst.EPTXECCHNCNTR.EPTXECPREEMPHASISMODE.offset) & 0x3
        etx_ec_pes = (lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR.address) >> lpgbtInst.EPTXECCHNCNTR.EPTXECPREEMPHASISSTRENGTH.offset) & 0x7
        etx_ec_pew = (lpgbtInst.read_reg(
            lpgbtInst.EPTXCONTROL.address) >> lpgbtInst.EPTXCONTROL.EPTXECPREEMPHASISWIDTH.offset) & 0x3
        eptx_invert = (lpgbtInst.read_reg(
            lpgbtInst.EPTXCONTROL.address) >> lpgbtInst.EPTXCONTROL.EPTXECINVERT.offset) & lpgbtInst.EPTXCONTROL.EPTXECINVERT.bit_mask
        etx_ec_ds = lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR.address) & lpgbtInst.EPTXECCHNCNTR.EPTXECDRIVESTRENGTH.bit_mask

    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        eptx_ec_enable = (lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR.address) >> lpgbtInst.EPTXECCHNCNTR.EPTXECENABLE.offset) & lpgbtInst.EPTXECCHNCNTR.EPTXECENABLE.bit_mask
        etx_ec_pem = (lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR2.address) >> lpgbtInst.EPTXECCHNCNTR2.EPTXECPREEMPHASISMODE.offset) & 0x3
        etx_ec_pes = lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR2.address) & lpgbtInst.EPTXECCHNCNTR2.EPTXECPREEMPHASISSTRENGTH.bit_mask
        etx_ec_pew = (lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR2.address) >> lpgbtInst.EPTXECCHNCNTR2.EPTXECPREEMPHASISWIDTH.offset)
        eptx_invert = (lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR.address) >> lpgbtInst.EPTXECCHNCNTR.EPTXECINVERT.offset) & 0x1
        etx_ec_ds = lpgbtInst.read_reg(
            lpgbtInst.EPTXECCHNCNTR.address) >> lpgbtInst.EPTXECCHNCNTR.EPTXECDRIVESTRENGTH.offset

    return {
        'eptx_ec_enable': eptx_ec_enable,
        'etx_ec_ds': etx_ec_ds,
        'etx_ec_pem': etx_ec_pem,
        'etx_ec_pes': etx_ec_pes,
        'etx_ec_pew': etx_ec_pew,
        'eptx_invert': eptx_invert,
    }


@ synchDecorator.synch
def put_eptx(lpgbtInst):
    """
        PUT request to set EPTX group global configuration
        Parameters : - Data rate
                     - Driving Strength (0-4 mA)
                     - Pre Emphasis Strength (0-4 mA)
                     - Pre Emphasis Mode (Disabled - Self Timed - Clock timed)
                     - Pre Emphasis Width ( Pulse length: 120 ps to 960 ps)
    """
    if not request.json:
        raise Exception('JSON field is not present')

    eptx_data_rate = json_get(request, "eptx_data_rate", 0)
    etx_ds = json_get(request, "etx_ds", 0)
    etx_pem = json_get(request, "etx_pem", 0)
    etx_pes = json_get(request, "etx_pes", 0)
    etx_pew = json_get(request, "etx_pew", 0)

    for group_id in range(0, 4):

        if eptx_data_rate == 0:
            # channel active none
            lpgbtInst.eptx_group_setup(
                group_id, eptx_data_rate, False, False, False, False, False)
        elif eptx_data_rate == 1:
            # channel active 0,1,2 and 3
            lpgbtInst.eptx_group_setup(
                group_id, eptx_data_rate, True, True, True, True, False)
        elif eptx_data_rate == 2:
            # channel active 0 and 2
            lpgbtInst.eptx_group_setup(
                group_id, eptx_data_rate, True, False, True, False, False)
        elif eptx_data_rate == 3:
            lpgbtInst.eptx_group_setup(
                group_id, eptx_data_rate, True, False, False, False, False)       # channel active 0

        for channel_id in range(0, 4):
            lpgbtInst.eptx_channel_config(
                group_id, channel_id, etx_ds, etx_pem, etx_pes, etx_pew, False)

    return {'eptx mode': mode}


@ synchDecorator.synch
def get_eptx(lpgbtInst):
    """
        GET EPTX group register configuration
    """

    eptx_data_rate = lpgbtInst.read_reg(
        lpgbtInst.EPTXDATARATE) >> lpgbtInst.EPTXDATARATE.EPTX0DATARATE.offset & 0x3
    etx_ds = lpgbtInst.read_reg(
        lpgbtInst.EPTX00CHNCNTR.address) >> lpgbtInst.EPTX00CHNCNTR.EPTX00DRIVESTRENGTH.offset & 0x7
    etx_pem = lpgbtInst.read_reg(
        lpgbtInst.EPTX00CHNCNTR.address) >> lpgbtInst.EPTX00CHNCNTR.EPTX00PREEMPHASISMODE.offset & 0x3
    etx_pes = lpgbtInst.read_reg(
        lpgbtInst.EPTX00CHNCNTR.address) >> lpgbtInst. EPTX00CHNCNTR.EPTX00PREEMPHASISSTRENGTH.offset & 0x7
    etx_pew = lpgbtInst.read_reg(lpgbtInst.EPTX01_00CHNCNTR.address) & 0x7

    return {
        'eptx_data_rate': eptx_data_rate,
        'etx_ds': etx_ds,
        'etx_pem': etx_pem,
        'etx_pes': etx_pes,
        'etx_pew': etx_pew,
    }


@ synchDecorator.synch
def put_eptx_advanced(lpgbtInst):
    """
        PUT request to set EPTX group advanced configuration
        Parameters : - Data rate
                     - Driving Strength (0-4 mA)
                     - Pre Emphasis Strength (0-4 mA)
                     - Pre Emphasis Mode (Disabled - Self Timed - Clock timed)
                     - Pre Emphasis Width ( Pulse length: 120 ps to 960 ps)
    """
    if not request.json:
        raise Exception('JSON field is not present')

    eptx_data_rate_group = json_get(request, "eptx_data_rate_group", 0)
    eptx_mirror = json_get(request, "eptx_mirror", 0)
    eptx_chns_enable = json_get(request, "eptx_chns_enable", 0)
    eptx_ds_chns = json_get(request, "eptx_ds_chns", 0)
    eptx_pem_chns = json_get(request, "eptx_pem_chns", 0)
    eptx_pes_chns = json_get(request, "eptx_pes_chns", 0)
    eptx_pew_chns = json_get(request, "eptx_pew_chns", 0)

    for group_id in range(0, 4):
        lpgbtInst.eptx_group_setup(group_id, eptx_data_rate_group[group_id], eptx_chns_enable[group_id][0],
                                   eptx_chns_enable[group_id][1], eptx_chns_enable[group_id][2], eptx_chns_enable[group_id][3], eptx_mirror[group_id])
        for channel_id in range(0, 4):
            lpgbtInst.eptx_channel_config(group_id, channel_id, eptx_ds_chns[group_id][channel_id], eptx_pem_chns[
                                          group_id][channel_id], eptx_pes_chns[group_id][channel_id], eptx_pew_chns[group_id][channel_id], False)

    return {'eptx advanced mode': mode}


@ synchDecorator.synch
def get_eptx_advanced(lpgbtInst):
    """
        GET EPTX group register configuration
    """

    eptx_data_rate_group = []
    eptx_mirror = []
    drive_strength = []
    pre_emphasis_mode = []
    pre_emphasis_strength = []
    pre_emphasis_width = []
    eptx_chns_enable = []
    eptx_ds_chns = []
    eptx_pem_chns = []
    eptx_pes_chns = []
    eptx_pew_chns = []
    enable = []
    for group_id in range(4):
        data_rate_reg = lpgbtInst.read_reg(lpgbtInst.EPTXDATARATE)
        eptx_data_rate_group.append((data_rate_reg >> (group_id*2)) & 0x3)

        enable_reg = lpgbtInst.read_reg(
            lpgbtInst.EPTX10ENABLE.address + int(group_id/2))

        mirror = bool(data_rate_reg & (1 << group_id))
        eptx_mirror.append(mirror)
        for channel_id in range(4):

            if enable_reg & (1 << (group_id*4+channel_id) % 8):
                dataEn = True
            else:
                dataEn = False
            enable.append(dataEn)
            primary_reg_adr = lpgbtInst.EPTX00CHNCNTR.address + group_id*4 + channel_id
            primary_reg_val = lpgbtInst.read_reg(primary_reg_adr)
            drive_strength.append(
                (primary_reg_val >> lpgbtInst.EPTX00CHNCNTR.EPTX00DRIVESTRENGTH.offset) & 0x7)
            pre_emphasis_mode.append(
                (primary_reg_val >> lpgbtInst.EPTX00CHNCNTR.EPTX00PREEMPHASISMODE.offset) & 0x3)
            pre_emphasis_strength.append(
                (primary_reg_val >> lpgbtInst.EPTX00CHNCNTR.EPTX00PREEMPHASISSTRENGTH.offset) & 0x7)
            secondary_reg_adr = lpgbtInst.EPTX01_00CHNCNTR.address + \
                group_id*2 + int(channel_id/2)
            secondary_reg_val = lpgbtInst.read_reg(secondary_reg_adr)
            secondary_reg_val >>= 4*(channel_id % 2)
            pre_emphasis_width.append(secondary_reg_val & 0x7)

    eptx_chns_enable = [enable[i:i+4] for i in range(0, len(enable), 4)]
    eptx_ds_chns = [drive_strength[i:i+4]
                    for i in range(0, len(drive_strength), 4)]
    eptx_pem_chns = [pre_emphasis_mode[i:i+4]
                     for i in range(0, len(pre_emphasis_mode), 4)]
    eptx_pes_chns = [pre_emphasis_strength[i:i+4]
                     for i in range(0, len(pre_emphasis_strength), 4)]
    eptx_pew_chns = [pre_emphasis_width[i:i+4]
                     for i in range(0, len(pre_emphasis_width), 4)]

    return {
        'eptx_data_rate_group': eptx_data_rate_group,
        'eptx_mirror': eptx_mirror,
        'eptx_ds_chns': eptx_ds_chns,
        'eptx_pem_chns': eptx_pem_chns,
        'eptx_pes_chns': eptx_pes_chns,
        'eptx_pew_chns': eptx_pew_chns,
        'eptx_chns_enable': eptx_chns_enable,
    }


@ synchDecorator.synch
def put_psclocks(lpgbtInst):
    """
        PUT clocks generator setup:
            - Phase shifter clocks
            - Eport clocks
    """
    if not request.json:
        raise Exception('JSON field is not present')

    psclk_freq = json_get(request, "psclk_freq", 0)       # PSCLK frequency
    psclk_phase = json_get(request, "psclk_phase", 0)      # PSCLK delay
    enable_fine_tune = json_get(request, "enable_fine_tune", 0)
    psclk_ds = json_get(request, "psclk_ds", 0)
    psclk_pes = json_get(request, "psclk_pes", 0)
    psclk_pem = json_get(request, "psclk_pem", 0)
    psclk_pew = json_get(request, "psclk_pew", 0)

    for id_ in range(0, 4):
        lpgbtInst.phase_shifter_setup(
            id_,
            psclk_freq[id_],
            int(psclk_phase[id_]),
            bool(enable_fine_tune[id_]),
            int(psclk_ds[id_]),
            int(psclk_pes[id_]),
            int(psclk_pem[id_]),
            int(psclk_pew[id_])
        )

        if enable_fine_tune[id_]:
            dll_reset_value = 1 << id_
            # with fine phase shifting, the DLL needs to be reseted
            lpgbtInst.write_reg(lpgbtInst.RST2.address, dll_reset_value)
            lpgbtInst.write_reg(lpgbtInst.RST2.address, 0)

    return {'phase shifter clocks': psclk_freq}


@ synchDecorator.synch
def put_eclocks(lpgbtInst):
    """
        PUT clocks generator setup:
            - Phase shifter clocks
            - Eport clocks
    """
    if not request.json:
        raise Exception('JSON field is not present')

    eclk_freq = json_get(request, "eclk_freq", 0)
    eclk_ds = json_get(request, "eclk_ds", 0)
    eclk_pes = json_get(request, "eclk_pes", 0)
    eclk_pem = json_get(request, "eclk_pem", 0)
    eclk_pew = json_get(request, "eclk_pew", 0)
    eclk_inv = json_get(request, "eclk_inv", 0)

    for clk_id in range(0, 29):
        lpgbtInst.eclk_setup(
            clk_id,
            int(eclk_freq[clk_id]),
            int(eclk_ds[clk_id]),
            int(eclk_pes[clk_id]),
            int(eclk_pem[clk_id]),
            int(eclk_pew[clk_id]),
            bool(eclk_inv[clk_id])
        )

    return {}


@ synchDecorator.synch
def get_clocks(lpgbtInst):
    """
        GET clocks setup
    """
    psclk_freq = []
    psclk_phase = []
    psclk_ds = []
    psclk_pes = []
    psclk_pem = []
    psclk_pew = []
    enable_fine_tune = []

    eclk_freq = []
    eclk_ds = []
    eclk_pes = []
    eclk_pem = []
    eclk_pew = []
    eclk_inv = []

    for id_ in range(0, 4):
        config = lpgbtInst.read_reg(lpgbtInst.PS0CONFIG.address+3*id_)
        freq = config & 0x7

        psclk_freq.append(freq)
        delay = lpgbtInst.read_reg(lpgbtInst.PS0DELAY.address+3*id_)
        delay |= int(bool(config & lpgbtInst.PS0CONFIG.PS0DELAY.bit_mask)) << 8
        psclk_phase.append(delay)

        psclk_drive_strength = (
            config >> lpgbtInst.PS0CONFIG.PS0DRIVESTRENGTH.offset) & 0x7
        psclk_ds.append(psclk_drive_strength)

        psout_driver = lpgbtInst.read_reg(
            lpgbtInst.PS0OUTDRIVER.address + 3*id_)
        psclk_preemphasis_strength = (
            psout_driver >> lpgbtInst.PS0OUTDRIVER.PS0PREEMPHASISSTRENGTH.offset) & 0x7
        psclk_pes.append(psclk_preemphasis_strength)

        psclk_preemphasis_mode = (
            psout_driver >> lpgbtInst.PS0OUTDRIVER.PS0PREEMPHASISMODE.offset) & 0x3
        psclk_pem.append(psclk_preemphasis_mode)

        psclk_preemphasis_width = (
            psout_driver >> lpgbtInst.PS0OUTDRIVER.PS0PREEMPHASISWIDTH.offset) & 0x7
        psclk_pew.append(psclk_preemphasis_width)

        fine_tune = (
            config & lpgbtInst.PS0CONFIG.PS0ENABLEFINETUNE.bit_mask) >> 6
        enable_fine_tune.append(fine_tune)

        if(psclk_freq[id_] == 0 and psclk_ds[id_] == 0):
            psclk_ds[id_] = 4

    for clk_id in range(0, 29):
        config_high = lpgbtInst.read_reg(
            lpgbtInst.EPCLK0CHNCNTRH.address + 2*clk_id)
        config_low = lpgbtInst.read_reg(
            lpgbtInst.EPCLK0CHNCNTRL.address + 2*clk_id)

        freq = (config_high >> lpgbtInst.EPCLK0CHNCNTRH.EPCLK0FREQ.offset) & 0x7
        eclk_freq.append(freq)

        eclk_drive_strength = (
            config_high >> lpgbtInst.EPCLK0CHNCNTRH.EPCLK0DRIVESTRENGTH.offset) & 0x7
        eclk_ds.append(eclk_drive_strength)

        eclk_preemphasis_strength = (
            config_low >> lpgbtInst.EPCLK0CHNCNTRL.EPCLK0PREEMPHASISSTRENGTH.offset) & 0x7
        eclk_pes.append(eclk_preemphasis_strength)

        preemphasis_mode = (
            config_low >> lpgbtInst.EPCLK0CHNCNTRL.EPCLK0PREEMPHASISMODE.offset) & 0x3
        eclk_pem.append(preemphasis_mode)

        preemphasis_width = (
            config_low >> lpgbtInst.EPCLK0CHNCNTRL.EPCLK0PREEMPHASISWIDTH.offset) & 0x7
        eclk_pew.append(preemphasis_width)

        invert = (config_high &
                  lpgbtInst.EPCLK0CHNCNTRH.EPCLK0INVERT.bit_mask) >> 6
        eclk_inv.append(invert)

        if(eclk_freq[clk_id] == 0 and eclk_ds[clk_id] == 0):
            eclk_ds[clk_id] = 4

    return {
        'psclk_freq': psclk_freq,
        'psclk_phase': psclk_phase,
        'eclk_freq': eclk_freq,
        'psclk_ds': psclk_ds,
        'psclk_pes': psclk_pes,
        'psclk_pem': psclk_pem,
        'psclk_pew': psclk_pew,
        'enable_fine_tune': enable_fine_tune,
        'eclk_ds': eclk_ds,
        'eclk_pes': eclk_pes,
        'eclk_pem': eclk_pem,
        'eclk_pew': eclk_pew,
        'eclk_inv': eclk_inv,
    }


@ synchDecorator.synch
def set_line_driver(lpgbtInst):

    if not request.json:
        raise Exception('JSON field is not present')

    mod_current = json_get(request, "mod_current", 0)
    pre_current = json_get(request, "pre_current", 0)
    preemphasis = json_get(request, "preemphasis", 0)
    preemphasis_duration = json_get(request, "preemphasis_duration", 0)

    lpgbtInst.line_driver_setup(
        int(mod_current), preemphasis, preemphasis_duration, int(pre_current))
    return {}


@ synchDecorator.synch
def get_line_driver(lpgbtInst):
    """
        GET line driver
    """
    mod_current = 0

    regH = lpgbtInst.read_reg(lpgbtInst.LDCONFIGH)
    regL = lpgbtInst.read_reg(lpgbtInst.LDCONFIGL)
    if regH & lpgbtInst.LDCONFIGH.LDEMPHASISENABLE.bit_mask:
        preemphasis = 1
    else:
        preemphasis = 0

    mod_current = regH & 0b01111111
    pre_current = regL & 0b01111111

    preemphasis_duration = (regL >> 7) & 0b1

    return {
        'mod_current': mod_current,
        'pre_current': pre_current,
        'preemphasis': preemphasis,
        'preemphasis_duration': preemphasis_duration,
    }


@ synchDecorator.synch
def set_hs_polarity(lpgbtInst):

    if not request.json:
        raise Exception('JSON field is not present')

    hsout_polarity = json_get(request, "hsout_polarity", 0)
    hsin_polarity = json_get(request, "hsin_polarity", 0)

    lpgbtInst.high_speed_io_invert(hsout_polarity, hsin_polarity)

    return {}


@ synchDecorator.synch
def get_hs_polarity(lpgbtInst):

    hsout_polarity = (lpgbtInst.read_reg(
        lpgbtInst.CHIPCONFIG.address) >> 7) & 0b1
    hsin_polarity = (lpgbtInst.read_reg(
        lpgbtInst.CHIPCONFIG.address) >> 6) & 0b1

    return {
        'hsout_polarity': hsout_polarity,
        'hsin_polarity': hsin_polarity,
    }


@ synchDecorator.synch
def set_equalizer(lpgbtInst):
    """
        SET equalizer
    """

    if not request.json:
        raise Exception('JSON field is not present')

    attenuation = json_get(request, "attenuation", 0)
    cap = json_get(request, "cap", 0)
    res0 = json_get(request, "res0", 0)
    res1 = json_get(request, "res1", 0)
    res2 = json_get(request, "res2", 0)
    res3 = json_get(request, "res3", 0)

    lpgbtInst.equalizer_setup(attenuation, cap, res0, res1, res2, res3)

    return {}


@ synchDecorator.synch
def get_equalizer(lpgbtInst):
    """
        GET equalizer
    """

    eq_config = lpgbtInst.read_reg(lpgbtInst.EQCONFIG)
    eq_res = lpgbtInst.read_reg(lpgbtInst.EQRES)

    attenuation = eq_config >> lpgbtInst.EQCONFIG.EQATTENUATION.offset
    cap = eq_config & 0b11

    res0 = (eq_res >> lpgbtInst.EQRES.EQRES0.offset) & 0x03
    res1 = (eq_res >> lpgbtInst.EQRES.EQRES1.offset) & 0x03
    res2 = (eq_res >> lpgbtInst.EQRES.EQRES2.offset) & 0x03
    res3 = (eq_res >> lpgbtInst.EQRES.EQRES3.offset) & 0x03

    return {
        'attenuation': attenuation,
        'cap': cap,
        'res0': res0,
        'res1': res1,
        'res2': res2,
        'res3': res3,
    }


@ synchDecorator.synch
def put_vref(lpgbtInst):

    if not request.json:
        raise Exception('JSON field is not present')

    vref_enable = json_get(request, "vref_enable", 0)
    vref_tune = json_get(request, "vref_tune", 0)

    lpgbtInst.vref_enable(vref_enable, int(vref_tune))

    return {}


@ synchDecorator.synch
def get_vref(lpgbtInst):
    """
        GET vref
    """
    vref_tune = 0
    vref_tune_max = 63
    vref_enable_reg = lpgbtInst.read_reg(lpgbtInst.VREFCNTR.address)

    vref_enable = bool(
        vref_enable_reg & lpgbtInst.VREFCNTR.VREFENABLE.bit_mask)

    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        vref_tune_max = 63
        vref_tune = vref_enable_reg & 0b111111

    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        vref_tune_max = 255
        vref_tune_reg = lpgbtInst.read_reg(lpgbtInst.VREFTUNE.address)
        vref_tune = vref_tune_reg << lpgbtInst.VREFTUNE.VREFTUNE.offset

    return {
        'vref_tune': vref_tune,
        'vref_enable': vref_enable,
        'vref_tune_max': vref_tune_max,
    }


@ synchDecorator.synch
def put_vdac(lpgbtInst):
    """
        SET vdac
    """

    if not request.json:
        raise Exception('JSON field is not present')

    vdac_enable = json_get(request, "vdac_enable", 0)
    vdac_value = json_get(request, "vdac_value", 0)

    lpgbtInst.vdac_setup(int(vdac_value), vdac_enable)
    return {}


@ synchDecorator.synch
def get_vdac(lpgbtInst):
    """
        GET vdac
    """

    vdac_regh = lpgbtInst.read_reg(lpgbtInst.DACCONFIGH.address)
    vdac_regl = lpgbtInst.read_reg(lpgbtInst.DACCONFIGL.address)
    vdac_enable = bool(vdac_regh & lpgbtInst.DACCONFIGH.VOLDACENABLE.bit_mask)

    vdac_value = ((vdac_regh & 0b111111) << 8) | vdac_regl

    return {
        'vdac_enable': vdac_enable,
        'vdac_value': vdac_value,
    }


@ synchDecorator.synch
def put_cdac(lpgbtInst):
    """
        SET cdac
    """
    if not request.json:
        raise Exception('JSON field is not present')

    cdac_enable = json_get(request, "cdac_enable", 0)
    cdac_value = json_get(request, "cdac_value", 0)
    cdac_channels = json_get(request, "cdac_channels", 0)

    res = lpgbtInst.read_reg(lpgbtInst.CURDACCHN.address)
    for value in cdac_channels:
        res |= 1 << value

    lpgbtInst.cdac_setup(int(cdac_value), res, cdac_enable)

    return {}


@ synchDecorator.synch
def get_cdac(lpgbtInst):
    """
        GET cdac
    """
    cdac_channels = []
    cdac_reg = lpgbtInst.read_reg(lpgbtInst.DACCONFIGH.address)

    cdac_value = lpgbtInst.read_reg(lpgbtInst.CURDACVALUE.address)
    cdac_enable = bool(cdac_reg & lpgbtInst.DACCONFIGH.CURDACENABLE.bit_mask)
    cdac_ch_reg = lpgbtInst.read_reg(lpgbtInst.CURDACCHN.address)

    for id_ in range(0, 8):
        if cdac_ch_reg & (1 << id_):
            cdac_channels.append(id_)

    return {
        'cdac_enable': cdac_enable,
        'cdac_value': cdac_value,
        'cdac_channels': cdac_channels,
    }


@ synchDecorator.synch
def put_adc(lpgbtInst):
    """
        Setup ADC
    """
    if not request.json:
        raise Exception('JSON field is not present')

    adc_inn = json_get(request, "adc_inn", 0)
    adc_inp = json_get(request, "adc_inp", 0)
    adc_gain = json_get(request, "adc_gain", 0)

    lpgbtInst.adc_config(adc_inp, adc_inn, adc_gain)

    return {}


@ synchDecorator.synch
def get_adc(lpgbtInst):
    """
        GET ADC config
    """
    adc_conv = 0
    adc_gain = (lpgbtInst.read_reg(lpgbtInst.ADCCONFIG.address)
                >> lpgbtInst.ADCCONFIG.ADCGAINSELECT.offset) & 0x3

    adc_inn = (lpgbtInst.read_reg(lpgbtInst.ADCSELECT.address)
               >> lpgbtInst.ADCSELECT.ADCINNSELECT.offset) & 0xF
    adc_inp = (lpgbtInst.read_reg(lpgbtInst.ADCSELECT.address)
               >> lpgbtInst.ADCSELECT.ADCINPSELECT.offset)

    try:
        adc_conv = lpgbtInst.adc_convert()
    except:
        pass

    return {
        'adc_inn': adc_inn,
        'adc_inp': adc_inp,
        'adc_gain': adc_gain,
        'adc_conv': adc_conv,
    }


@ synchDecorator.synch
def get_internal_temp(lpgbtInst):
    """
        Configure ADC block to get internal sensor temperature.

         Params:
             ADC's positive input connected to the temperature sensor
             ADC's negative input connected to VREF/2
             VREF enabled
    """
    vref_reg = lpgbtInst.read_reg(lpgbtInst.VREFCNTR.address)

    if not(vref_reg & lpgbtInst.VREFCNTR.VREFENABLE.bit_mask):
        vref_value = (1 << 7)+vref_reg
    else:
        vref_value = vref_reg

    lpgbtInst.write_reg(lpgbtInst.VREFCNTR.address, vref_value)

    lpgbtInst.adc_config(lpgbtInst.AdcInputSelect.TEMP,
                         lpgbtInst.AdcInputSelect.VREF2, lpgbtInst.AdcGainSelect.X2)
    sensor_temp = round((lpgbtInst.adc_convert(1) - 486.2)/2.105, 1)
    return {
        'sensor_temp': sensor_temp,
    }


@ synchDecorator.synch
def put_vdd_mon(lpgbtInst):
    """
        Setup VDD monitoring :
        Configure ADC block and select VDDxxMonEnable bit.

         Params:
             ADC's positive input connected to VDD monitoring source
             ADC's negative input connected to VREF/2
    """

    if not request.json:
        raise Exception('JSON field is not present')

    vdd_mon = json_get(request, "vdd_mon", 0)

    lpgbtInst.write_reg(lpgbtInst.ADCMON.address, 0x1F)
    lpgbtInst.adc_config(
        vdd_mon, lpgbtInst.AdcInputSelect.VREF2, lpgbtInst.AdcGainSelect.X2)

    return {}


@ synchDecorator.synch
def write(lpgbtInst):
    newval = json_get(request, "newval", 0)
    address = json_get(request, "address", 0)

    lpgbtInst.write_reg(int(address, 16), int(newval, 16))

    return {}


@ synchDecorator.synch
def put_write_I2Cm(lpgbtInst):
    if not request.json:
        raise Exception('JSON field is not present')

    i2cm = json_get(request, "i2cm", 0)
    slave_address = json_get(request, "slave_address", 0)
    reg_address = json_get(request, "reg_address", 0)
    speed = json_get(request, "speed", 0)
    write_data = json_get(request, "write_data", 0)
    reg_address_width = json_get(request, "reg_address_width", 0)

    lpgbtInst.i2c_master_write(i2cm, int(slave_address, 16), reg_address_width, int(
        reg_address, 16), int(write_data, 16), speed)

    return {}


@ synchDecorator.synch
def put_read_I2Cm(lpgbtInst):
    if not request.json:
        raise Exception('JSON field is not present')

    i2cm = json_get(request, "i2cm", 0)
    slave_address = json_get(request, "slave_address", 0)
    reg_address = json_get(request, "reg_address", 0)
    speed = json_get(request, "speed", 0)
    reg_address_width = json_get(request, "reg_address_width", 0)

    data_read = lpgbtInst.i2c_master_read(i2cm, int(
        slave_address, 16), 1, reg_address_width, int(reg_address, 16), speed)

    return {
        'data_read': data_read,
    }


@ synchDecorator.synch
def put_reset_I2Cm(lpgbtInst):
    if not request.json:
        raise Exception('JSON field is not present')

    i2cm = json_get(request, "i2cm", 0)

    lpgbtInst.i2c_master_reset(i2cm)
    return {}


@ synchDecorator.synch
def get_i2c_master_read(lpgbtInst):

    read_i2cm0 = hex(lpgbtInst.read_reg(lpgbtInst.I2CM0READ15.address))
    read_i2cm1 = hex(lpgbtInst.read_reg(lpgbtInst.I2CM1READ15.address))
    read_i2cm2 = hex(lpgbtInst.read_reg(lpgbtInst.I2CM2READ15.address))

    return {
        'read_i2cm0': read_i2cm0,
        'read_i2cm1': read_i2cm1,
        'read_i2cm2': read_i2cm2,
    }


@ synchDecorator.synch
def regsm_save(lpgbtInst):
    response = ''

    if request.method == 'POST':
        i2cm = request.form['i2cm']
        slave_address = request.form['slave_address']
        reg_address_width = request.form['reg_address_width']

    data_read = []
    for address in lpgbtInst.Reg:
        data_read += lpgbtInst.i2c_master_read(
            master_id=int(i2cm),
            slave_address=int(
                slave_address, 16),
            read_len=1,
            reg_address_width=int(reg_address_width),
            reg_address=int(address),
            timeout=1,
            addr_10bit=False,
        )

    for i, v in enumerate(data_read):
        response += "%3d 0x%02X\n" % (i, v)

    filename = 'lpgbt_master_config_{}.cnf'.format(
        datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    with open('regs/'+filename, 'w') as output_file:
        output_file.write(response)

    return {'filename': filename}


def regsm_download(filename):
    return send_file('regs/'+filename, attachment_filename=filename, as_attachment=True)


@ synchDecorator.synch
def regsm_load(lpgbtInst):
    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ("cnf")

    if request.method == 'POST':
        i2cm = request.form['i2cm']
        slave_address = request.form['slave_address']
        reg_address_width = 2
        # check if the post request has the file part
        if 'config' not in request.files:
            raise Exception('No file part')
        file = request.files['config']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            raise Exception('No file selected')

        filename = secure_filename(file.filename)
        if file and allowed_file(filename):
            for l in file.stream.readlines():
                l = l.decode('utf-8').strip()
                if l.startswith("#"):
                    continue
                ll = l.split()
                if len(ll) >= 2:
                    addr = 0
                    val = 0
                    try:
                        addr = int(ll[0], 0)
                        val = int(ll[1], 0)
                    finally:
                        lpgbtInst.i2c_master_write(
                            master_id=int(i2cm),
                            slave_address=int(slave_address, 16),
                            reg_address_width=int(reg_address_width),
                            reg_address=addr,
                            data=val,
                            timeout=1,
                            addr_10bit=False,
                        )
            return {}

    raise Exception('Method is not allowed')


@ synchDecorator.synch
def put_gpio(lpgbtInst):
    """
        Put configuration of all IO pin

        Params:
            dir_: 16 bit number (dir_[n] = 0 pin n as input, dir_[n]=1 pin as output)
            ds  : 16 bit number (ds[n] = 0 pin n has low driving strength, ds[n]=1 pin n has high driving strength)
            out : 16 bit number (out[n] = 0 pin driven low, out[n]=1 pin driven high)
            pull: 16 bit number (pull[n] = 0 pull Up/Down resistor disconnected,
                                 pull[n] = -1 pull Up resistor connected,
                                 pull[n] = 1 pull Down resistor connected)
    """
    if not request.json:
        raise Exception('JSON field is not present')

    gpio_dir = json_get(request, "dir",  [0]*16)
    gpio_ds = json_get(request, "ds",   [0]*16)
    gpio_output = json_get(request, "out",  [0]*16)
    gpio_pull = json_get(request, "pull", [0]*16)

    pio_pullEn = []
    pio_UpDown = []

    for i in range(16):
        if gpio_pull[i] == -1:
            pio_pullEn.append(1)
            pio_UpDown.append(0)
        elif gpio_pull[i] == 1:
            pio_pullEn.append(1)
            pio_UpDown.append(1)
        elif gpio_pull[i] == 0:
            pio_pullEn.append(0)
            pio_UpDown.append(0)

    direction = int("".join(str(x) for x in gpio_dir[::-1]), 2)
    output = int("".join(str(x) for x in gpio_output[::-1]), 2)
    drive = int("".join(str(x) for x in gpio_ds[::-1]), 2)
    pullena = int("".join(str(x) for x in pio_pullEn[::-1]), 2)
    updown = int("".join(str(x) for x in pio_UpDown[::-1]), 2)

    lpgbtInst.gpio_set_dir(direction)
    lpgbtInst.gpio_set_out(output)
    lpgbtInst.gpio_set_drive(drive)
    lpgbtInst.gpio_set_pullena(pullena)
    lpgbtInst.gpio_set_updown(updown)

    return {}


@ synchDecorator.synch
def get_gpio(lpgbtInst):
    """
        Get GPIO configuration from GPIO registers

        Params:
            dir_: PIODIRH:dir_[15:8] and PIODIRL:dir_[7:0] - 16 bit number (dir_[n] = 0 pin n as input, dir_[n]=1 pin as output)
            ds  : PIODRIVESTRENGTHH:ds[15:8] and PIODRIVESTRENGTHL:ds[7:0] - 16 bit number (ds[n] = 0 pin n has low driving strength, ds[n]=1 pin n has high driving strength)
            out : PIOOUTH:out[15:8] and PIOOUTL:out[7:0] - 16 bit number (out[n] = 0 pin driven low, out[n]=1 pin driven high)
            pull: PIOPULLENAH:pull[15:8] and PIOPULLENAL:pull[7:0] - 16 bit number (pull[n] = 0 pull Up/Down resistor disconnected,
                                 pull[n] = -1 pull Up resistor connected,
                                 pull[n] = 1 pull Down resistor connected)
    """
    gpio_dir = []
    gpio_out = []
    gpio_ds = []
    gpio_pull = []

    for id_ in range(0, 16):

        direction = (lpgbtInst.gpio_get_dir() >> id_) & 0b1
        gpio_dir.append(direction)

        output = (lpgbtInst.gpio_get_out() >> id_) & 0b1
        gpio_out.append(output)

        ds = (lpgbtInst.gpio_get_drive() >> id_) & 0b1
        gpio_ds.append(ds)

    pullena = [int(i)
               for i in list('{0:016b}'.format(lpgbtInst.gpio_get_pullena()))][::-1]
    gpio_in = [int(i)
               for i in list('{0:016b}'.format(lpgbtInst.gpio_get_in()))][::-1]

    for id_ in range(0, 16):
        if pullena[id_] == 0 and gpio_in[id_] == 0:
            gpio_pull.append(0)
        elif pullena[id_] == 1 and gpio_in[id_] == 0:
            gpio_pull.append(-1)
        elif pullena[id_] == 1 and gpio_in[id_] == 1:
            gpio_pull.append(1)
        else:
            gpio_pull.append(0)

    return {
        'gpio_dir': gpio_dir,
        'gpio_out': gpio_out,
        'gpio_ds': gpio_ds,
        'gpio_pull': gpio_pull,
        'gpio_in': gpio_in,
    }


@ synchDecorator.synch
def regs_get(lpgbtInst):
    """ Read and log all register values """
    result = []
    for address in lpgbtInst.Reg:
        data = lpgbtInst.read_reg(address)
        data_bin = '{0:08b}'.format(data)
        name = lpgbtInst.Reg(address).name,
        reg_name = "%-28s" % name
        result.append({"Address": hex(address),
                      "Name": reg_name, "Value": "0x%02X" % data})
    return result


@ synchDecorator.synch
def regs_save(lpgbtInst):
    response = ''
    reg_val = []
    reg_address = []

    for address in lpgbtInst.Reg:
        reg_val.append(lpgbtInst.read_reg(address))
        reg_address.append("0x%03X" % (address))

    for i, v in enumerate(reg_val):
        response += "%s 0x%02X\n" % (reg_address[i], v)
    filename = 'lpgbt_config_{}.cnf'.format(
        datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    with open('regs/'+filename, 'w') as output_file:
        output_file.write(response)

    return {'filename': filename}


def regs_download(filename):
    return send_file('regs/'+filename, attachment_filename=filename, as_attachment=True)


@ synchDecorator.synch
def regs_load(lpgbtInst):
    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ("cnf")

    if request.method == 'POST':
        # check if the post request has the file part
        if 'config' not in request.files:
            raise Exception('No file part')
        file = request.files['config']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            raise Exception('No file selected')

        filename = secure_filename(file.filename)
        if file and allowed_file(filename):
            for l in file.stream.readlines():
                l = l.decode('utf-8').strip()
                if l.startswith("#"):
                    continue
                ll = l.split()
                if len(ll) >= 2:
                    addr = 0
                    val = 0
                    try:
                        addr = int(ll[0], 0)
                        val = int(ll[1], 0)
                    finally:
                        lpgbtInst.write_reg(addr, val)
            return {}

    raise Exception('Method is not allowed')


@ synchDecorator.synch
def put_data_sources(lpgbtInst):
    """

        Put LpGBT test pattern to transmit : a fixed pattern, PRBS sequence, clock sequence, or loop back incoming downlink frame
        The data pattern to the serializer 'ser' is controlled by ULSerTestPattern[3:0] field in ULDataSource0 register :

        Params:
                DATA	        Normal mode of operation
                PRBS7	        PRBS7 test pattern (x7 + x6 + 1)
                PRBS15	        PRBS15 test pattern (x15 + x14 + 1)
                PRBS23	        PRBS23 test pattern (x23 + x18 + 1)
                PRBS31	        PRBS31 test pattern (x31 + x28 + 1)
                CLK5G12	        5.12 GHz clock pattern (in 5Gbps mode it will produce only 2.56 GHz)
                CLK2G56	        2.56 GHz clock pattern
                CLK1G28	        1.28 GHz clock pattern
                CLK40M	        40 MHz clock pattern
                DLFRAME_10G24	Loop back, downlink frame repeated 4 times
                DLFRAME_5G12	Loop back, downlink frame repeated 2 times, each bit repeated 2 times
                DLFRAME_2G56	Loop back, downlink frame repeated 1 times, each bit repeated 4 times
                CONST PATTERN	8 x DPDataPattern[31:0]

    """
    if not request.json:
        raise Exception('JSON field is not present')

    ser = json_get(request, "ser", 0)
    eprx = json_get(request, "eprx", 0)
    ld_source = json_get(request, "ld_source", 0)
    eptx = json_get(request, "eptx", 0)
    eprx_gen = json_get(request, "eprx_gen", 0)
    const_pattern = json_get(request, "const_pattern", 0)

    bert_source = json_get(request, "bert_source", 0)
    bert_chn_source = json_get(request, "bert_chn_source", 0)
    meas_time = json_get(request, "meas_time", 0)
    skip_cycle = json_get(request, "skip_cycle", 0)
    bert_start = json_get(request, "bert_start", 0)

    source = ((bert_source << 4) + ((bert_chn_source & 0xF) >> 0))

    lpgbtInst.line_driver_set_data_source(ld_source)
    lpgbtInst.serializer_set_data_source(ser)

    lpgbtInst.uplink_set_constant_pattern(int(const_pattern, 16))
    lpgbtInst.bert_set_constant_pattern(int(const_pattern, 16))

    for group_id in range(4):
        lpgbtInst.downlink_set_data_source(group_id, eptx[group_id])
    for group_id in range(7):
        if ser == 0:
            lpgbtInst.uplink_set_data_source(group_id, eprx[group_id])
        for channel_id in range(4):
            if channel_id in eprx_gen[group_id]:
                lpgbtInst.eprx_prbs_gen_enable(group_id, channel_id, True)
            else:
                lpgbtInst.eprx_prbs_gen_enable(group_id, channel_id, False)

    lpgbtInst.bert_config(meas_time, source, skip_cycle)

    return {}


@ synchDecorator.synch
def get_data_sources(lpgbtInst):
    """
        GET LpGBT test pattern configuration
    """
    status = 0
    ser = lpgbtInst.read_reg(lpgbtInst.ULDATASOURCE0.address)
    eprx = []
    eptx = []
    eprx_gen = [['', '', '', ''], ['', '', '', ''], ['', '', '', ''], [
        '', '', '', ''], ['', '', '', ''], ['', '', '', ''], ['', '', '', '']]

    bert_source = lpgbtInst.read_reg(lpgbtInst.BERTSOURCE.address) >> 4
    bert_chn_source = (lpgbtInst.read_reg(
        lpgbtInst.BERTSOURCE.address) & 0xF) >> 0

    meas_time = lpgbtInst.read_reg(
        lpgbtInst.BERTCONFIG.address) >> lpgbtInst.BERTCONFIG.BERTMEASTIME.offset
    ld_source = lpgbtInst.read_reg(
        lpgbtInst.ULDATASOURCE1.address) >> lpgbtInst.ULDATASOURCE1.LDDATASOURCE.offset
    skip_cycle = (lpgbtInst.read_reg(lpgbtInst.BERTCONFIG.address)
                  & lpgbtInst.BERTCONFIG.SKIPDISABLE.bit_mask) >> 1

    pattern0 = lpgbtInst.read_reg(lpgbtInst.DPDATAPATTERN0.address) & 0xff
    pattern1 = lpgbtInst.read_reg(lpgbtInst.DPDATAPATTERN1.address) & 0xff
    pattern2 = lpgbtInst.read_reg(lpgbtInst.DPDATAPATTERN2.address) & 0xff
    pattern3 = lpgbtInst.read_reg(lpgbtInst.DPDATAPATTERN3.address) & 0xff

    const_pattern = hex((pattern0 << 24) + (pattern1 << 16) +
                        (pattern2 << 8) + (pattern3 << 0))

    for group_id in range(4):
        eptx_reg_addr = lpgbtInst.ULDATASOURCE5.address
        eptx_val = lpgbtInst.read_reg(eptx_reg_addr)
        eptx.append((eptx_val & (3 << (group_id)*2)) >> (group_id % 4)*2)

    for group_id in range(7):
        eprx_reg_addr = lpgbtInst.ULDATASOURCE1.address + int(group_id/2)
        eprx_val = lpgbtInst.read_reg(eprx_reg_addr)
        eprx.append((eprx_val & (7 << (group_id % 2)*3)) >> ((group_id % 2)*3))

        for channel_id in range(4):
            id_ = channel_id + group_id*4
            reg = lpgbtInst.EPRXPRBS0.address - int(id_/8)
            eprx_gen_val = lpgbtInst.read_reg(reg) & (
                lpgbtInst.EPRXPRBS1.EPRX20PRBSENABLE.bit_mask << (group_id*4+channel_id) % 8)
            if eprx_gen_val > 0:
                eprx_gen[group_id][channel_id] = channel_id

    errors = lpgbtInst.read_reg(lpgbtInst.BERTRESULT0.address) | \
        lpgbtInst.read_reg(lpgbtInst.BERTRESULT1.address) << 8 | \
        lpgbtInst.read_reg(lpgbtInst.BERTRESULT2.address) << 16 | \
        lpgbtInst.read_reg(lpgbtInst.BERTRESULT3.address) << 24 | \
        lpgbtInst.read_reg(lpgbtInst.BERTRESULT4.address) << 32

    return {
        'ser': ser,
        'eprx': eprx,
        'eptx': eptx,
        'eprx_gen': eprx_gen,
        'bert_source': bert_source,
        'bert_chn_source': bert_chn_source,
        'meas_time': meas_time,
        'ld_source': ld_source,
        'const_pattern': const_pattern,
        'errors': errors,
        'skip_cycle': skip_cycle,
    }


@ synchDecorator.synch
def get_bert(lpgbtInst):
    """
       Get BER test information.
    """
    result = lpgbtInst.bert_run()

    return {
        'errors': result,
    }


@ synchDecorator.synch
def get_process_monitor(lpgbtInst):
    """
        GET process monitor status
    """
    pm_frequency = []

    try:
        for i in range(4):
            pm_frequency.append(lpgbtInst.get_process_monitor(i))
    except:
        for i in range(4):
            pm_frequency.append(0)

    return {
        'pm_frequency': pm_frequency,
    }


@ synchDecorator.synch
def get_bypass_datapath(lpgbtInst):
    """
        GET uplink and downlink datapath
    """
    uplink_path = []
    downlink_path = []
    data = lpgbtInst.read_reg(lpgbtInst.DATAPATH.address)

    for b_id in range(0, 4):
        if data & (1 << (b_id % 8)):
            uplink_path.append(b_id)

    for b_id in range(5, 8):
        if data & (1 << (b_id % 8)):
            downlink_path.append(b_id)

    return {
        'uplink_path': uplink_path,
        'downlink_path': downlink_path,
    }


@ synchDecorator.synch
def put_bypass_datapath(lpgbtInst):
    """
        PUT uplink and downlink datapath
    """
    if not request.json:
        raise Exception('JSON field is not present')

    uplink_path = json_get(request, "uplink_path", 0)
    downlink_path = json_get(request, "downlink_path", 0)
    data_path = uplink_path + downlink_path
    value = lpgbtInst.read_reg(lpgbtInst.DATAPATH.address)

    for b_id in range(8):
        if b_id in data_path:
            value |= 1 << (b_id % 8)
            lpgbtInst.write_reg(lpgbtInst.DATAPATH.address, value)
        else:
            value &= ~(1 << (b_id % 8))
            lpgbtInst.write_reg(lpgbtInst.DATAPATH.address, value)

    return {}


@ synchDecorator.synch
def get_test_outputs(lpgbtInst):
    """
        GET test output
    """
    testsel = []
    testoutputs_ds = []

    diff_ds = []
    diff_pem = []
    diff_pew = []
    diff_pes = []

    diff_pew.append((lpgbtInst.read_reg(lpgbtInst.TOPREEMP.address)
                    >> lpgbtInst.TOPREEMP.TO4PREEMPHASISWIDTH.offset) & 0x7)
    diff_pew.append((lpgbtInst.read_reg(lpgbtInst.TOPREEMP.address)
                    >> lpgbtInst.TOPREEMP.TO5PREEMPHASISWIDTH.offset) & 0x7)

    for reg_id in range(2):
        diff_ds.append((lpgbtInst.read_reg(lpgbtInst.TO4DRIVER.address+reg_id)
                       >> lpgbtInst.TO4DRIVER.TO4DRIVESTRENGTH.offset) & 0x7)
        diff_pem.append((lpgbtInst.read_reg(lpgbtInst.TO4DRIVER.address+reg_id)
                        >> lpgbtInst.TO4DRIVER.TO4PREEMPHASISMODE.offset) & 0x3)
        diff_pes.append((lpgbtInst.read_reg(lpgbtInst.TO4DRIVER.address+reg_id)
                        >> lpgbtInst.TO4DRIVER.TO4PREEMPHASISSTRENGTH.offset) & 0x7)

    for reg_id in range(6):
        reg = lpgbtInst.read_reg(lpgbtInst.TO0SEL.address + reg_id)
        testsel.append(reg)

    for reg_id in range(4):
        value = (lpgbtInst.read_reg(
            lpgbtInst.TODRIVINGSTRENGTH.address) >> reg_id) & 0b1
        testoutputs_ds.append(value)

    return {
        'testsel': testsel,
        'testoutputs_ds': testoutputs_ds,
        'diff_ds': diff_ds,
        'diff_pem': diff_pem,
        'diff_pew': diff_pew,
        'diff_pes': diff_pes,
    }


@ synchDecorator.synch
def put_test_outputs(lpgbtInst):
    """
        PUT test output
    """

    if not request.json:
        raise Exception('JSON field is not present')

    testsel = json_get(request, "testsel", 0)
    cmos_ds = json_get(request, "testoutputs_ds", 0)
    diff_ds = json_get(request, "diff_ds", 0)
    diff_pem = json_get(request, "diff_pem", 0)
    diff_pew = json_get(request, "diff_pew", 0)
    diff_pes = json_get(request, "diff_pes", 0)

    for reg_id in range(0, 4):
        lpgbtInst.cmos_test_output_setup(
            reg_id, testsel[reg_id], cmos_ds[reg_id])

    for reg_id in range(4, 6):
        lpgbtInst.differential_test_output_setup(
            reg_id, testsel[reg_id],  diff_ds[reg_id-4], diff_pes[reg_id-4], diff_pem[reg_id-4], diff_pew[reg_id-4])

    return {}


@ synchDecorator.synch
def get_watchdog_config(lpgbtInst):
    """
        GET watch dog PLL/DLL locked signal
    """
    wd_pll_enable = True
    wd_dll_enable = True
    wd_pll_timeout = 0

    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        wd_reg = lpgbtInst.read_reg(lpgbtInst.POWERUP0)
        wd_pll_enable = not (bool(
            wd_reg & lpgbtInst.POWERUP0.PUSMPLLWDOGDISABLE.bit_mask))
        wd_pll_timeout = wd_reg & 0xF

    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        wd_reg = lpgbtInst.read_reg(lpgbtInst.WATCHDOG)
        wd_pll_enable = not (bool(
            wd_reg & lpgbtInst.WATCHDOG.PUSMPLLWDOGDISABLE.bit_mask))
        wd_dll_enable = not(bool(
            wd_reg & lpgbtInst.WATCHDOG.PUSMDLLWDOGDISABLE.bit_mask))

    return {
        'wd_pll_enable': wd_pll_enable,
        'wd_dll_enable': wd_dll_enable,
        'wd_pll_timeout': wd_pll_timeout
    }


@synchDecorator.synch
def put_watchdog_config(lpgbtInst):
    """
        Disables watch dog monitoring PLL locked signal
        Disables watch dog monitoring DLL locked signal
    """
    if not request.json:
        raise Exception('JSON field is not present')

    wd_pll_enable = json_get(request, "wd_pll_enable", 0)
    wd_dll_enable = json_get(request, "wd_dll_enable", 0)
    wd_pll_timeout = json_get(request, "wd_pll_timeout", 0)

    if lpgbtInst.read_reg(0x1c5) == 0xa5:
        lpgbtInst.pll_watchdog_config(
            wd_pll_enable=wd_pll_enable, wd_pll_timeout=wd_pll_timeout)
        wd_reg = lpgbtInst.read_reg(lpgbtInst.POWERUP0)
        wd_pll_enable = not (bool(
            wd_reg & lpgbtInst.POWERUP0.PUSMPLLWDOGDISABLE.bit_mask))
        wd_pll_timeout = wd_reg & 0xF

    elif lpgbtInst.read_reg(0x1d7) == 0xa6:
        lpgbtInst.config_watchdog(pll_watchdog_enable=wd_pll_enable,
                                  dll_watchdog_enable=wd_dll_enable,
                                  )
    return {}


@synchDecorator.synch
def put_reset_watchdog(lpgbtInst):
    """
        Reset he number of PLL/DLL watchdog action by executing a write access
    """

    pusm_pll_watchdog_counter = lpgbtInst.read_reg(
        lpgbtInst.PUSMDLLWATCHDOG.address)
    pusm_dll_watchdog_counter = lpgbtInst.read_reg(
        lpgbtInst.PUSMPLLWATCHDOG.address)

    if pusm_pll_watchdog_counter != 0:
        lpgbtInst.write_reg(lpgbtInst.PUSMDLLWATCHDOG.address, 0)

    if pusm_dll_watchdog_counter != 0:
        lpgbtInst.write_reg(lpgbtInst.PUSMPLLWATCHDOG.address, 0)

    return {}


@synchDecorator.synch
def put_reset_timeout(lpgbtInst):
    """
        Reset he number of PLL/DLL timeout action by executing a write access
    """

    pusm_pll_timeout_counter = lpgbtInst.read_reg(
        lpgbtInst.PUSMPLLTIMEOUT.address)
    pusm_dll_timeout_counter = lpgbtInst.read_reg(
        lpgbtInst.PUSMDLLTIMEOUT.address)

    if pusm_pll_timeout_counter != 0:
        lpgbtInst.write_reg(lpgbtInst.PUSMPLLTIMEOUT.address, 0)

    if pusm_dll_timeout_counter != 0:
        lpgbtInst.write_reg(lpgbtInst.PUSMDLLTIMEOUT.address, 0)

    return {}


init_routes()
