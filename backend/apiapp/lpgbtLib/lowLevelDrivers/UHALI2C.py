import logging

class I2C:

    def __init__(self):
        logging.warning('UHAL interface is not supported yet')

    def write_byte(self,addr,value):
        raise Exception('UHAL interface is not supported yet')

    def write_i2c_block_data(self,addr,regl,t_values):
        raise Exception('UHAL interface is not supported yet')

    def read_byte(self,addr):
        raise Exception('UHAL interface is not supported yet')

    def read_i2c_block_data(self,offset,registeradd,len):
        raise Exception('UHAL interface is not supported yet')

    def to_dict(self):
        return {}

    def from_dict(self, dict):
        logging.debug('Nothing to do...')