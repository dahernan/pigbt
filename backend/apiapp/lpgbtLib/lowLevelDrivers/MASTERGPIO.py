import logging

class GPIO:

    def __init__(self, lpgbtLowLevelInst):
        self.pins = {}
        self.lpgbtLowLevelInst = lpgbtLowLevelInst

    def set_pin(self, gpio_pin, value):
        self.pins[gpio_pin] = value


    def get_pin(self, gpio_pin):
        if gpio_pin in self.pins:
            return self.pins[gpio_pin]

        else:
            return False #By default, all pins are set to false

    def to_dict(self):
        return self.pins

    def from_dict(self, dict):
        self.pins = dict


    def get_pins(self, device_addr):
        return self.pins

    def set_pins(self, pins):
        self.pins = pins

