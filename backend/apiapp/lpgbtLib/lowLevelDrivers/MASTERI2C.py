import logging
from .RPII2C import I2C as RPIi2c

from lpgbt_control_lib import LpgbtV1

Lpgbt = LpgbtV1


class I2C:

    def __init__(self, dev_id=1):
        self.i2c = RPIi2c()
        self.logger = logging.getLogger("lpgbt")
        self.lpgbt = Lpgbt(logger=self.logger)
        self.lpgbt_address = None
        self.slave_address = None
        self.instantiate_device_level_drivers()

    def instantiate_device_level_drivers(self):
        """ Assigns all drivers for device-level entities"""

        self.lpgbt.register_comm_intf(
            name="I2C",
            write_regs=self.write_lpgbt_regs_i2c,
            read_regs=self.read_lpgbt_regs_i2c,
            default=True
        )

    # i2c control
    def write_lpgbt_regs_i2c(self, reg_addr, reg_vals):
        """Writes register(s) via tester I2C interface."""
        self.write_regs(
            device_addr=self.lpgbt_address,
            reg_addr=reg_addr,
            reg_vals=reg_vals
        )

    def read_lpgbt_regs_i2c(self, reg_addr, read_len):
        """Reads register(s) via tester I2C interface."""
        values = self.read_regs(
            device_addr=self.lpgbt_address,
            reg_addr=reg_addr,
            read_len=read_len
        )
        return values

    def write_byte(self, addr, value):
        if self.i2c is None:
            raise Exception('SMBUS is not supported')

        self.i2c.write_byte(addr, value)

    def write_i2c_block_data(self, addr, regl, t_values):
        if self.i2c is None:
            raise Exception('SMBUS is not supported')

        self.i2c.write_i2c_block_data(addr, regl, t_values)

    def read_byte(self, addr):
        if self.i2c is None:
            raise Exception('SMBUS is not supported')

        return self.i2c.read_byte(addr)

    def read_i2c_block_data(self, offset, registeradd, len):
        if self.i2c is None:
            raise Exception('SMBUS is not supported')

        return self.i2c.read_i2c_block_data(offset, registeradd, len)

    def read_regs(self, device_addr, reg_addr, read_len):
        regl = (reg_addr & 0xFF) >> 0
        regh = (reg_addr) >> 8

        t_values = [regh]

        self.write_i2c_block_data(device_addr, regl, t_values)
        data = self.i2c.bus.read_i2c_block_data(
            0, reg_addr, read_len)
        return data

    def write_regs(self, device_addr, reg_addr, reg_vals):
        regl = (reg_addr & 0xFF) >> 0
        regh = (reg_addr) >> 8
        t_values = [regh] + reg_vals

        self.write_i2c_block_data(device_addr, regl, t_values)

    def lpgbt_write(self, device_addr, reg_addr, reg_vals, **fnargs):
        self.lpgbt_address = device_addr
        i2cm = list(fnargs.values())[0]
        self.slave_address = list(fnargs.values())[1]

        print('')
        print('in lpgbt_write')
        print(self.slave_address, list(fnargs.values())
              [1], type(self.slave_address))
        print('')

        self.lpgbt.i2c_master_write(
            master_id=i2cm,
            slave_address=int(self.slave_address),
            reg_address_width=2,
            reg_address=reg_addr,
            data=reg_vals,
            timeout=1,
            addr_10bit=False,
        )

    def lpgbt_reads(self, device_addr, reg_addr, read_len, **fnargs):

        self.lpgbt_address = device_addr
        i2cm = list(fnargs.values())[0]
        self.slave_address = list(fnargs.values())[1]

        print('')
        print('in lpgbt_reads')
        print(self.slave_address, list(fnargs.values())
              [1], type(self.slave_address))
        print('')

        read_value = self.lpgbt.i2c_master_read(
            master_id=i2cm,
            slave_address=int(self.slave_address),
            read_len=read_len,
            reg_address_width=2,
            reg_address=reg_addr,
            timeout=1,
            addr_10bit=False,
        )

        return read_value

    def to_dict(self):
        return {}

    def from_dict(self, dict):
        logging.debug('Nothing to do...')
