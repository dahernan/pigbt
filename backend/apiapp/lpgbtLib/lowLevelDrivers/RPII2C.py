import logging

try:
    import smbus
except Exception as e:
    logging.warning('SMBUS import: {}'.format(e))


class I2C:

    def __init__(self, dev_id=1):
        try:
            self.bus = smbus.SMBus(dev_id)
        except:
            self.bus = None
            pass

    def write_byte(self, addr, value):
        if self.bus is None:
            raise Exception('SMBUS is not supported')

        self.bus.write_byte(addr, value)

    def write_i2c_block_data(self, addr, regl, t_values):
        if self.bus is None:
            raise Exception('SMBUS is not supported')

        self.bus.write_i2c_block_data(addr, regl, t_values)

    def read_byte(self, addr):
        if self.bus is None:
            raise Exception('SMBUS is not supported')

        return self.bus.read_byte(addr)

    def read_i2c_block_data(self, offset, registeradd, len):
        if self.bus is None:
            raise Exception('SMBUS is not supported')

        return self.bus.read_i2c_block_data(offset, registeradd, len)

    def lpgbt_reads(self, device_addr, reg_addr, read_len):
        regl = (reg_addr & 0xFF) >> 0
        regh = (reg_addr) >> 8

        t_values = [regh]

        self.write_i2c_block_data(device_addr, regl, t_values)
        data = self.bus.read_i2c_block_data(
            0, reg_addr, read_len)  # self.read_byte(addr)
        return data

    def lpgbt_write(self, device_addr, reg_addr, reg_vals):
        regl = (reg_addr & 0xFF) >> 0
        regh = (reg_addr) >> 8
        t_values = [regh] + reg_vals

        self.write_i2c_block_data(device_addr, regl, t_values)

    def to_dict(self):
        return {}

    def from_dict(self, dict):
        logging.debug('Nothing to do...')
