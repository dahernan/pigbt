import random
import time
import logging
import numpy as np
import matplotlib.pyplot as plt

from .lpgbt_low_level import LpgbtLowLevel as LpgbtLowLevel
from lpgbt_control_lib import LpgbtV1, LpgbtV0

try:
    import smbus
except Exception as e:
    logging.warning('SMBUS import: {}'.format(e))


class PiGBT():

    def __init__(self, lpgbtLowLevelInst):
        """Constructor for piGBT class, assigns all drivers"""
        try:
            self.bus = smbus.SMBus(1)
        except:
            self.bus = None
            pass

        self.lpgbtLowLevel = lpgbtLowLevelInst
        self.lpgbt_address = self.lpgbtLowLevel.get_i2c_address()
        self.chip_version = self.detect_chip_version()

    def write_i2c_block_data(self, addr, regl, t_values):
        if self.bus is None:
            raise Exception('SMBUS is not supported')

        self.bus.write_i2c_block_data(addr, regl, t_values)

    def read_byte(self, addr):
        if self.bus is None:
            raise Exception('SMBUS is not supported')

        return self.bus.read_byte(addr)

    def lpgbt_read(self, addr, reg):
        regl = (reg & 0xFF) >> 0
        regh = (reg) >> 8
        t_values = [regh]
        self.write_i2c_block_data(addr, regl, t_values)
        data = self.read_byte(addr)

        return data

    def detect_chip_version(self):

        lpgbt_address = 0
        chip_version = self.lpgbtLowLevel.get_chip_version()

        try:
            bus_address = self.scan_smbus()
            lpgbt_address = bus_address[0]
            if self.lpgbt_read(lpgbt_address, 0x1c5) == 0xa5:
                chip_version = 0
            elif self.lpgbt_read(lpgbt_address, 0x1d7) == 0xa6:
                chip_version = 1
        except:
            pass

        if chip_version == 1:
            self.lpgbt = LpgbtV1(logger=logging.getLogger("lpgbt"))
            self.instantiate_device_level_drivers()

        elif chip_version == 0:
            self.lpgbt = LpgbtV0(logger=logging.getLogger("lpgbt"))

            self.instantiate_device_level_drivers()

        return chip_version

    def instantiate_device_level_drivers(self):
        """ Assigns all drivers for device-level entities"""

        self.lpgbt.register_comm_intf(
            name="I2C",
            write_regs=self.write_lpgbt_regs_i2c,
            read_regs=self.read_lpgbt_regs_i2c,
            default=True
        )
        self.lpgbt.register_ctrl_pin_access(
            write_pin=self.write_lpgbt_ctrl_pin,
            read_pin=self.read_lpgbt_ctrl_pin
        )

    # i2c control
    def write_lpgbt_regs_i2c(self, reg_addr, reg_vals):
        """Writes register(s) via tester I2C interface."""
        self.write_regs(
            device_addr=self.lpgbt_address,
            reg_addr=reg_addr,
            reg_vals=reg_vals
        )

    def read_lpgbt_regs_i2c(self, reg_addr, read_len):
        """Reads register(s) via tester I2C interface."""
        values = self.read_regs(
            device_addr=self.lpgbt_address,
            reg_addr=reg_addr,
            read_len=read_len
        )
        return values

    # gpio control
    def write_lpgbt_ctrl_pin(self, pin_name, value):
        """Read rpi control pin"""
        _RPI_GPIO_MAP = {

            self.lpgbt.CTRL_MODE0: 4,
            self.lpgbt.CTRL_MODE1: 5,
            self.lpgbt.CTRL_MODE2: 6,
            self.lpgbt.CTRL_MODE3: 7,

            self.lpgbt.CTRL_ADDR0: 8,
            self.lpgbt.CTRL_ADDR1: 9,
            self.lpgbt.CTRL_ADDR2: 10,
            self.lpgbt.CTRL_ADDR3: 11,

            self.lpgbt.CTRL_RSTB: 12,

            self.lpgbt.CTRL_LOCKMODE: 14,

            self.lpgbt.CTRL_PORDIS: 16,
            self.lpgbt.CTRL_READY: 18,
            self.lpgbt.CTRL_RSTOUTB: 19,
        }
        gpio_id = _RPI_GPIO_MAP[pin_name]
        self.lpgbtLowLevel.set_pin(gpio_id, value)

    def read_lpgbt_ctrl_pin(self, pin_name):
        """Read rpi control pin"""
        return self.lpgbtLowLevel.get_pin(pin_name)

    # low level interface control
    def getLpgbtLowLevelInst(self):
        return self.lpgbtLowLevel

    def setLpgbtLowLevelInst(self, lpgbtLowLevel):
        self.lpgbtLowLevel = lpgbtLowLevel

    def run_emulator(self):
        self.lpgbtLowLevel.run_emulator()

    def feast_control(self, state):  # control vldb+ FEASTMPs

        self.lpgbtLowLevel.set_pin(LpgbtLowLevel.CTRL_PORDIS, state)

        if state == 0:
            self.lpgbtLowLevel.set_platform(2)

    def control_fuse_pin(self, state):  # control 2.5V fusing pin
        self.lpgbtLowLevel.set_pin(LpgbtLowLevel.CTRL_FUSE0, state)
        self.lpgbtLowLevel.set_pin(LpgbtLowLevel.CTRL_FUSE1, 0)

    def get_feast_control(self):  # status of the vldb+ FEASTMPs
        return self.lpgbtLowLevel.get_pin(LpgbtLowLevel.CTRL_PORDIS)

    def scan_smbus(self):
        devices = []
        for device_addr in range(1, 128):
            try:
                self.lpgbtLowLevel.read_byte(device_addr)
                devices.append(device_addr)
            except Exception as e:  # exception if read_byte fails
                pass
        return devices

    def write_regs(self, device_addr, reg_addr, reg_vals):

        try:
            self.lpgbtLowLevel.lpgbt_write(device_addr, reg_addr, reg_vals)
        except Exception as e:
            self.lpgbtLowLevel.set_platform(2)
            raise Exception(
                'Communication with the lpGBT lost. Platform set on Dummy mode. Go to Connection to search for a new device')
            pass

    def read_regs(self, device_addr, reg_addr, read_len):
        data = []

        try:
            data = self.lpgbtLowLevel.lpgbt_reads(
                device_addr, reg_addr, read_len)
        except Exception as e:
            pass

        return data

    def rpi_control_pin(self, pin, value):
        self.lpgbtLowLevel.set_pin(pin, value)
