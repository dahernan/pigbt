#sudo apt-get install python3-venv
#cd venv/lib/python3.6/site-packages
#ln -s /usr/lib/python3.6/tkinter tkinter
#virtualenv venv

python3 -m venv venv
. venv/bin/activate
pip install --upgrade pip
pip install Flask Flask-Menu Flask-Nav
pip install numpy scipy matplotlib enum
pip install -U flask-cors
pip install uwsgi
pip install smbus
pip install bitarray
pip install RPi.GPIO

#pip install waitress
#export FLASK_APP=webapp
#export FLASK_ENV=development
#flask run
#flask  run --host=128.141.49.133  --no-reload
#waitress-serve --call 'apiapp:create_app'
# iptables -A IN_public_allow -p tcp --dport 8000 -m state --state NEW -j ACCEPT
# scl enable rh-python36 bash
