""" Constants"""

class OledConstants(object):
	""" Class containing all Raspberry Pi oled screen constants"""

	# Constants
	LCD_I2C_ADDRESS         = 0x3C    # 011110+SA0
	# 128*64 pixel
	LCD_WIDTH               = 0x80  
	LCD_HEIGHT              = 0x40 

	LCD_SETHIGHCOLUMN       = 0x10    # set Higher Column Start Address for Page Addressing Mode 
	LCD_SETLOWCOLUMN        = 0x00
	LCD_MEMORYMODE          = 0x20    # set Memory Addressing Mode 
	LCD_COLUMNADDR          = 0x21    # set Column Address 

	LCD_SETCONTRAST         = 0x81    # set contrst control of bank 0

	LCD_CHARGEPUMP          = 0x8D    # charge Pump Setting 
	LCD_EN_CHARGEPUMP       = 0x14    # enable Charge Pump 

	LCD_SEGREMAP            = 0xA1    # set segment re-map

	LCD_FULLDISPLAYON       = 0xA4    # entire Display ON 
	LCD_DISPLAYALLON        = 0xA5
	LCD_NORMALDISPLAY       = 0xA6    # set Normal Display
	LCD_INVERTDISPLAY       = 0xA7    # set Invert Display
	LCD_MULTIPLEXRATIO      = 0xA8    # set multiplex Ratio
	LCD_DISPLAYOFF          = 0xAE    # set Display OFF
	LCD_DISPLAYON           = 0xAF    # set Display ON
	LCD_PAGEADDR            = 0x22    # set Page Address

	LCD_COMSCANDIR          = 0xC8    # set COM Output Scan Direction
	LCD_COMSCANINC          = 0xC0    # set COM Output Scan Direction

	LCD_SETSTARTLINE        = 0x40    # set Display Start Line 
	LCD_OFFSET              = 0x0
	LCD_SETDISPLAYOFFSET    = 0xD3    # set Display Offset
	LCD_SETDISPLAYCLOCKDIV  = 0xD5    # set Display Clock Divide Ratio/ Oscillator Frequency 
	LCD_SETPRECHARGE        = 0xD9    # set Pre-charge Period
	LCD_SETCOMCONFIG        = 0xDA    # set COM Pins Hardware Configuration 
	LCD_SETVCOMADJUST       = 0xDB    # set VCOMH Deselect Level 
	LCD_NOP                 = 0xE3    # no opetation command

	# Graphic acceleration command
	LCD_RIGHTHORIZONTALSCROLL      = 0x26 # horizontal Scroll Setup
	LCD_LEFTHORIZONTALSCROLL       = 0x27
	LCD_VERTICALR_HORIZONTALSCROLL = 0x29 # continuous Vertical and Horizontal Scroll Setup 
	LCD_VERTICALL_HORIZONTALSCROLL = 0x2A

	LCD_ACTIVATESCROLL             = 0x2F # activate Scroll       
	LCD_DEACTIVATESCROLL           = 0x2E # deactivate Scroll 
	LCD_VERTICALSCROLLAREA         = 0xA3 # set Vertical Scroll Area

	LCD_FRQ                 = 0x80    # set osc frequency
	LCD_DUTY                = 0x1F
	LCD_ALTCOM              = 0x12    # set altenative COM 
	LCD_EXTERNALVCC         = 0x1
	LCD_SWITCHCAPVCC        = 0x2

