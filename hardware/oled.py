import smbus
from .oled_constants import OledConstants

#default I2C bus number
bus = smbus.SMBus(0)

class oled(OledConstants):
	"""Class to define SSD1306 OLED displays functions .
	"""

	def __init__(self):

		self._i2c    = OledConstants.LCD_I2C_ADDRESS
		self.width   = OledConstants.LCD_WIDTH
		self.height  = OledConstants.LCD_HEIGHT
		self._pages  = OledConstants.LCD_HEIGHT//8
		self._buffer = [0]*(OledConstants.LCD_WIDTH*self._pages)

	def init_driver(self,vccstate=OledConstants.LCD_SWITCHCAPVCC):
		"""Initialize SSD1306 driver"""
		self._vccstate = vccstate

		# 128x64 pixel specific initialization.
		self.write_cmd(OledConstants.LCD_DISPLAYOFF)           #set display off
		self.write_cmd(OledConstants.LCD_SETDISPLAYCLOCKDIV)   #set display clock
		self.write_cmd(OledConstants.LCD_FRQ)                  #105 Hz
			
		self.write_cmd(OledConstants.LCD_MULTIPLEXRATIO)       #select multiplex ratio
		self.write_cmd(OledConstants.LCD_DUTY)                 #0->0x3F  

		self.write_cmd(OledConstants.LCD_SETDISPLAYOFFSET)     #setting display offset
		self.write_cmd(OledConstants.LCD_OFFSET)               #00 set common start

		self.write_cmd(OledConstants.LCD_SETSTARTLINE)         #set display start line

		self.write_cmd(OledConstants.LCD_CHARGEPUMP)           #set charge pump

		if self._vccstate == OledConstants.LCD_EXTERNALVCC:
			self.write_cmd(0x10)
		else:
			self.write_cmd(OledConstants.LCD_EN_CHARGEPUMP)    #enable charge pump

		self.write_cmd(OledConstants.LCD_MEMORYMODE)
		self.write_cmd(0x00)

		self.write_cmd(OledConstants.LCD_SEGREMAP)             #set segment remap col addr 127 mapped to 0

		self.write_cmd(OledConstants.LCD_COMSCANDIR)           #set COM output scan direction

		self.write_cmd(OledConstants.LCD_SETCOMCONFIG)         #set COM hardware configuration
		self.write_cmd(0x02)                                   #set alternative COM pin

		self.write_cmd(OledConstants.LCD_SETCONTRAST)          #set contrast control
		self.write_cmd(0x8F)

		self.write_cmd(OledConstants.LCD_SETPRECHARGE)         # set pre charge period
				     
		if self._vccstate == OledConstants.LCD_EXTERNALVCC:
			self.write_cmd(0x22)                              # pre charge val
		else:
			self.write_cmd(0xF1)

		self.write_cmd(OledConstants.LCD_SETVCOMADJUST)       # deselect vcomh level
		self.write_cmd(0x40)

		self.write_cmd(OledConstants.LCD_FULLDISPLAYON)        # entire display ON
		self.write_cmd(OledConstants.LCD_NORMALDISPLAY)                          # normal display
		self.write_cmd(OledConstants.LCD_DISPLAYON)            # display ON

	def write_cmd(self,value):
		"""Write an 8-bit data to oled register"""
		register = 0x00
		value = value & 0xFF
		bus.write_byte_data(OledConstants.LCD_I2C_ADDRESS, register, value)

	def write_multi(self, register, data):
		"""Write n bytes to oled register"""
		bus.write_i2c_block_data(OledConstants.LCD_I2C_ADDRESS, register, data)

	def clear(self):
		self._buffer = [0]*(self.width*self._pages)          #Clear image buffer

	def display_buffer(self):
		"""Write display buffer to physical display."""
		self.write_cmd(OledConstants.LCD_COLUMNADDR)
		self.write_cmd(0)              # Column start address
		self.write_cmd(self.width-1)   # Column end address
		self.write_cmd(OledConstants.LCD_PAGEADDR)
		self.write_cmd(0)              # Page start address
		self.write_cmd(self._pages-1)  # Page end address
		# Write buffer data

		for i in range(0, len(self._buffer), 16):
			control = 0x40
			self.write_multi(control, self._buffer[i:i+16])

	def image(self, display):
		"""Set buffer to value of Python Imaging Library image
		"""
		if display.mode != '1':
			raise ValueError('Set mode 1')
		image_width, image_height = display.size
		if image_width != self.width or image_height != self.height:
			raise ValueError('display must be : ({0}x{1}).' \
				.format(self.width, self.height))
		# Grab all the pixels from the image, faster than getpixel
		pixel = display.load()
		# Iterate through the memory pages
		index = 0
		for page in range(self._pages):
			# Iterate through all x axis columns
			for x in range(self.width):
				# Set the bits for the column of pixels at the current position
				bits = 0
				# Don't use range here as it's a bit slow
				for bit in [0, 1, 2, 3, 4, 5, 6, 7]:
					bits = bits << 1
					bits |= 0 if pixel[(x, page*8+7-bit)] == 0 else 1
				# Update buffer byte and increment to next byte
				self._buffer[index] = bits
				index += 1
