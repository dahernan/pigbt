import RPi.GPIO as GPIO
import time
import socket
import os
import fcntl
import struct
import subprocess
import importlib.util

from hardware import oled
from hardware import wifi

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


class Rpi():

    WIFI_SWITCH = 7
    DHCP_SWITCH = 17
    SYSTEM_RESTART_BUTTON = 26
    APP_LED2_GREEN = 18
    SYSTEM_LED3_GREEN = 24
    SYSTEM_LED3_RED = 25
    LPGBT_LED1_GREEN = 14
    LPGBT_LED1_RED = 15

    def synch_repo(self):

        time.sleep(5)

        os.system('git pull')
        current_tag = subprocess.check_output(
            ['git describe --tags'], shell=True).strip().decode()
        fetch_tag = subprocess.check_output(
            ['git tag | tail -1'], shell=True).strip().decode()

        if current_tag != fetch_tag:

            subprocess.call('git reset --hard HEAD', shell=True)
            self.display("UPDATING PiGBT", "reloading", "files wait ...", " ")

            subprocess.call('git checkout tags/' + fetch_tag, shell=True)
            subprocess.call(
                'cd frontend && npm run build -- --mode staging', shell=True)
            self.display("", "rebooting", " ", " ")
            time.sleep(1)
            os.system('sudo reboot')

    def init_gpio(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        # pin 17 and 7 as input
        GPIO.setup(self.WIFI_SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.DHCP_SWITCH, GPIO.IN)
        GPIO.setup(self.SYSTEM_RESTART_BUTTON, GPIO.IN)

        # intialize reset pin
        GPIO.setup(9, GPIO.OUT)
        GPIO.output(9, GPIO.HIGH)

        # read mode pin
        GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(6, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        state_mode0 = GPIO.input(13)
        state_mode1 = GPIO.input(6)
        state_mode2 = GPIO.input(5)
        state_mode3 = GPIO.input(11)

        # force mode
        GPIO.setup(13, GPIO.OUT)
        GPIO.output(13, state_mode0)
        GPIO.setup(6, GPIO.OUT)
        GPIO.output(6, state_mode1)
        GPIO.setup(5, GPIO.OUT)
        GPIO.output(5, state_mode2)
        GPIO.setup(11, GPIO.OUT)
        GPIO.output(11, state_mode3)

    def inst_pypackage(self):
        "Installing required python packages"

        subprocess.call('pip install git+https://gitlab.cern.ch/lpgbt/lpgbt_control_lib.git@v4.0.0', shell=True)

    def init_pi(self):
        current_tag = subprocess.check_output(
            ['git describe --tags'], shell=True).strip().decode()
        self.display("    Starting", " ", "    PiGBT " + current_tag, "")
        GPIO.setup(self.SYSTEM_LED3_GREEN, GPIO.OUT)
        GPIO.output(self.SYSTEM_LED3_GREEN, GPIO.HIGH)

        GPIO.setup(self.APP_LED2_GREEN, GPIO.OUT)
        GPIO.output(self.APP_LED2_GREEN, GPIO.HIGH)

        subprocess.call('rm eye/*', shell=True)
        subprocess.call('rm regs/*', shell=True)

        time.sleep(2)
        state = GPIO.input(self.WIFI_SWITCH)
        if(state == 1):
            self.wifiEnableCallback(self.WIFI_SWITCH)
        else:
            self.dhcpModeCallback(self.DHCP_SWITCH)

    def get_interface_ipaddress(self, network):
        ip = 'no network'
        time.sleep(3)
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            ip = (socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915,
                  struct.pack('256s', network[:15].encode('utf-8')))[20:24]))
        except:
            pass
        return ip

    def wifiEnableCallback(self, channel):
        if (GPIO.input(channel)):
            ip_address = ''
            os.system('sudo systemctl start dnsmasq')
            os.system('sudo systemctl start hostapd')
            time.sleep(3)
            wifi_ssid = (subprocess.check_output(
                ['sed', '-n', '3p;41q', '/etc/hostapd/hostapd.conf'])).decode()
            wifi_password = ((subprocess.check_output(
                ['sed', '-n', '11p;41q', '/etc/hostapd/hostapd.conf'])).decode())[15:25]

            while not ip_address:
                self.display("", "Activating WiFi ...", "", "")
                try:
                    ip_address = self.get_interface_ipaddress('wlan0')
                except:
                    pass
            self.display("IP:"+str(ip_address)+":8080", wifi_ssid,
                         "password: "+wifi_password, "")
        else:
            self.display("WIFI OFF", "checking ", "connection ...", " ")
            os.system('sudo systemctl stop dnsmasq')
            os.system('sudo systemctl stop hostapd')
            time.sleep(3)
            self.dhcpModeCallback(self.DHCP_SWITCH)

    def dhcpModeCallback(self, channel):
        ip_address = self.get_interface_ipaddress('eth0')
        if (GPIO.input(channel)):
            message = "DHCP MASTER"
            os.system('sudo ip link set eth0 down')
            os.system('sudo ip link set eth0 up')

            os.system(
                'sudo ip addr add 192.168.0.2/255.255.255.0 broadcast 192.168.255.255 dev eth0')
            os.system('sudo ip route add default via 192.168.0.0')
            os.system('sudo systemctl stop dnsmasq')
            os.system('sudo systemctl start isc-dhcp-server')
            ip_address = self.get_interface_ipaddress('eth0')
        else:
            message = "DHCP SLAVE"
            os.system('sudo systemctl stop dnsmasq')
            os.system('sudo ip link set eth0 down')
            os.system('sudo ip link set eth0 up')
            os.system('sudo systemctl daemon-reload')
            os.system('sudo dhclient eth0')

        self.display(message, "IP: "+str(ip_address), "PORT: 8080", "")

    def display(self, line1, line2, line3, line4):
        display = oled.oled()

        # Initialize SSD1306
        display.init_driver()

        # Clear display.
        display.clear()
        display.display_buffer()

        # set empty image
        width = display.width
        height = display.height
        image = Image.new('1', (width, height))
        ImageDraw.Draw(image).text((10, 0), line1,
                                   font=ImageFont.load_default(), fill=255)
        ImageDraw.Draw(image).text((10, 8), line2,
                                   font=ImageFont.load_default(), fill=255)
        ImageDraw.Draw(image).text((10, 16), line3,
                                   font=ImageFont.load_default(), fill=255)
        ImageDraw.Draw(image).text((10, 24), line4,
                                   font=ImageFont.load_default(), fill=255)

        # Display image.
        display.image(image)
        display.display_buffer()

    def resetButtonInterrupt(self, channel):
        global start
        global end
        if GPIO.input(self.SYSTEM_RESTART_BUTTON) == 1:
            start = time.time()
        if GPIO.input(self.SYSTEM_RESTART_BUTTON) == 0:
            end = time.time()
            elapsed = end - start
            if elapsed >= 3:
                self.display("", "REGENERATING", "  WiFI ...", "")
                network = wifi.wifi()
                network.update_wifi()
                self.wifiEnableCallback(self.WIFI_SWITCH)
            elif elapsed >= 1:
                GPIO.setup(self.SYSTEM_LED3_RED, GPIO.OUT)   # APP
                GPIO.output(self.SYSTEM_LED3_RED, GPIO.HIGH)
                self.display('', 'Reboot', 'Raspberry pi', '')
                time.sleep(2)

                GPIO.setup(self.SYSTEM_LED3_GREEN, GPIO.OUT)   # SYSTEM
                GPIO.output(self.SYSTEM_LED3_GREEN, GPIO.LOW)
                GPIO.setup(self.APP_LED2_GREEN, GPIO.OUT)   # APP
                GPIO.output(self.APP_LED2_GREEN, GPIO.LOW)

                GPIO.setup(self.LPGBT_LED1_GREEN, GPIO.OUT)   # LPGBT
                GPIO.output(self.LPGBT_LED1_GREEN, GPIO.LOW)

                GPIO.setup(self.LPGBT_LED1_RED, GPIO.OUT)   # LPGBT
                GPIO.output(self.LPGBT_LED1_RED, GPIO.LOW)

                self.display('', '', '', '')
                os.system('sudo reboot')


if __name__ == "__main__":

    rpi = Rpi()

    rpi.init_gpio()
    rpi.inst_pypackage()
    rpi.synch_repo()

    GPIO.add_event_detect(rpi.DHCP_SWITCH, GPIO.BOTH,
                          callback=rpi.dhcpModeCallback)
    GPIO.add_event_detect(rpi.WIFI_SWITCH, GPIO.BOTH,
                          callback=rpi.wifiEnableCallback)
    GPIO.add_event_detect(rpi.SYSTEM_RESTART_BUTTON, GPIO.BOTH,
                          callback=rpi.resetButtonInterrupt, bouncetime=200)

    rpi.init_pi()

    while(True):
        time.sleep(1)
