# PiGBT Application

The Low-Power Gigabit Transceiver (lpGBT) is a radiation tolerant ASIC for multipurpose high-speed bidirectional optical links in HEP experiments.
Its data interface to the detectors’ frontends is highly configurable supporting multiple data rates. The lpGBT is designed to be used for data acquisition as well as for trigger and timing distribution.
It also features control interfaces and environmental monitoring functions to implement experiment control.

Therefore, this device contains a huge register map to be configured and to being monitored in real-time.

The privous GBTx component configuration was made using a JAVA application and a custom USB to I2C dongle designed at CERN.
Nevertheless, that implied some limitations:
*  No remote control
*  Static interface
*  Custom electronics
*  OS compatibility
*  No GPIOs available

For these reasons, the RasperryPi was selected as the best candidate to be the new programmer system.

<img src="https://cernbox.cern.ch/index.php/s/AXiBYmaqOgnB55X/download" />

## How to setup

This application requires the following packages to be installed on the rasperyPi:
*  Python3
*  python3-pip
*  npm

And the following python libraries:
*  gunicorn==19.9.0
*  Flask
*  gevent
*  Flask-Menu
*  Flask-Nav
*  numpy
*  scipy
*  matplotlib
*  flask-cors
*  smbus
*  RPi.GPIO

Finally, the app.sh script execute the deployment sequence and the application can be easily accessed using a web browser.

## Project architecture

The project is divided in two parts, frontend and backend, as shown in the diagram below.

<img src='https://cernbox.cern.ch/index.php/s/rdJaEBXYfw0d4uY/download' />

The **FrontEnd** folder contains the views, meaning the web pages that communicate with the backend application using JSON. <br/>
The **BackEnd** folder contains the /api route handler and call dedicated lpgbt functions depending on the request.
