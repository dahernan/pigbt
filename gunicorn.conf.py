# file gunicorn.conf.py
# coding=utf-8
import os
import multiprocessing

loglevel = 'info'

errorlog = "-"
accesslog = "-"

# bind = 'unix:%s' % os.path.join(_VAR, 'run/gunicorn.sock')
bind = '0.0.0.0:8080'
worker_class = 'gevent'
capture_output = True
