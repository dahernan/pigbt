#!/bin/bash

# Test that npm works correctly

# Create a package.json file so npm doesn't throw off warnings
echo "Initializing npm and testing the installation and use of a module ..."
cd frontend

# mkdirp chosen because it's on the list of 'most depended-upon packages':
#   https://www.npmjs.com/browse/depended
npm update
npm install
npm run build -- --mode staging
npm rebuild

cd ..
mkdir -p eye
mkdir -p regs
gunicorn wsgi -k gevent --bind=0.0.0.0:8080 --access-logfile=- --config "$APP_CONFIG"
